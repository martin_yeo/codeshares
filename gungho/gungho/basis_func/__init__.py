"""Apply basis functions to a cell."""


import numpy as np
import matplotlib.pyplot as plt

import basis_function as basis


def plot_1d(x, cell, solutions, point=None):
    """
    Apply a reference cell to a cell and plot the result at the desired
    resolution defined by `npoints`.

    """
    for name, solution in solutions.iteritems():
        plt.plot(x, solution, label=name)
    if point is not None:
        point_solutions = cell.solve(point)
        plt.axvline(point, color='k', ls='--')
        ypoints = [v[0] for v in point_solutions.values()]
        xpoints = [point] * len(ypoints)
        plt.scatter(xpoints, ypoints)
    plt.legend(loc='best')
    plt.title('1D basis functions applied to a {}'.format(str(cell)))
    plt.show()


def plot_2d(x, y, cell, solutions, key='combined', point=None, multiple=False):
    """
    Apply a reference cell to a cell and plot the result at the desired
    resolution defined by `npoints`.

    """
    if not multiple:
        ax = plt.axes(aspect='equal')
        # ax.set_ylim(bottom=cell.j0 - 2)
        # ax.set_xlim(bottom=cell.i0 - 2)
        ax.margins(0.15)
    contours = len(x)
    solution = solutions[key]
    plt.contourf(x, y, solution, n=contours)
    plot_cell_extents(cell)
    title = '2D basis functions applied to a {}\nPlotting {!r}'
    plt.title(title.format(str(cell), key))
    if point is not None:
        pass
    if not multiple:
        plt.show()


def plot_cell_extents(cell):
    """
    Plots the extent of a cell as a black line.

    """
    # Cell bottom line.
    plt.axhline(y=cell.j0, xmin=cell.i0, xmax=cell.i1, color='k', zorder=1)
    # Cell top line.
    plt.axhline(y=cell.j1, xmin=cell.i0, xmax=cell.i1, color='k', zorder=1)
    # Cell left line.
    plt.axvline(x=cell.i0, ymin=cell.j0, ymax=cell.j1, color='k', zorder=1)
    # Cell right line.
    plt.axvline(x=cell.i1, ymin=cell.j0, ymax=cell.j1, color='k', zorder=1)


def do_1d():
    # Sample cells.
    # c = Cell(2, 3, -1, [3, 6])
    c = basis.Cell3DoF(0.25, 6, 1.34, basis.RefCell1D3DoF, [-2, 4])
    # c = Cell(2, 3, -1, [0, 1])

    # Data.
    npoints = 25
    x = np.linspace(c.i0, c.i1, npoints)
    point = 1.4
    solutions = c.solve(x)
    # print solutions

    # Plotting
    plot_1d(x, c, solutions, point=point)


def do_2d():
    # Sample cells.
    # c2d = Cell(0.25, 6, 1.34, [0, 1], y_extents=[0, 1])
    c2d = basis.Cell3DoF(0.25, 6, 1.34, basis.RefCell2D3DoF, [3, 5],
                         y_extents=[2, 7])

    # Data.
    npoints = 25
    x = np.linspace(c2d.i0, c2d.i1, npoints)
    y = np.linspace(c2d.j0, c2d.j1, npoints)

    # Solutions.
    solutions = c2d.solve(x, y)

    # Plotting
    plot_2d(x, y, c2d, solutions, key='f(x)')
    plot_2d(x, y, c2d, solutions, key='g(x)')
    plot_2d(x, y, c2d, solutions, key='h(x)')
    plot_2d(x, y, c2d, solutions, key='combined')


def main():
    # do_1d()
    do_2d()


if __name__ == '__main__':
    main()
