"""
Apply a set of 1D basis functions to a 1D cell (an edge).

"""

import numpy as np


class RefCell1D3DoF(object):
    interval = [0, 1]

    def __init__(self, x):
        """
        Create a 1D reference cell with three basis functions defined.

        Inputs:
            * x: The input values, normalised into the interval (0, 1).

        """
        self.x = self._to_array(x)

    @staticmethod
    def _to_array(itm):
        if not hasattr(itm, '__iter__'):
            itm = np.array([itm])
        return itm

    def f(self, a):
        """Define function `f(x) = 2a` for single coefficient a."""
        return np.zeros(len(self.x)) + 2*a

    def g(self, b):
        """Define function `g(x) = bx` for single coefficient b."""
        return b * self.x

    def h(self, c):
        """Define function `h(x) = 2x^2 + c` for single coefficient c."""
        return 2 * (self.x**2) + c

    def combined(self, a, b, c):
        """Return the sum of the functions f(x), g(x), h(x)."""
        return self.f(a) + self.g(b) + self.h(c)


class RefCell2D3DoF(object):
    interval = [0, 1]

    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.x2d, self.y2d = self._generate_2d_coords()

    def _generate_2d_coords(self):
        """Convert linear x and y into meshed x2d and y2d."""
        return np.meshgrid(self.x, self.y)

    def f(self, a):
        """Define function `f(x, y) = x + 3y - a` for single coefficient a."""
        return self.x2d + 3 * self.y2d - a

    def g(self, b):
        """Define function `g(x, y) = xy + b` for single coefficient b."""
        return self.x2d * self.y2d + b

    def h(self, c):
        """Define func `h(x, y) = 0.5x^2 + 3y + c` for single coefficient c."""
        return 0.5 * (self.x2d**2) + 3 * self.y2d + c

    def combined(self, a, b, c):
        return self.f(a) + self.g(b) + self.h(c)


class RefCell2D5DoF(object):
    interval = [0, 1]

    def __init__(self, x, y):
        self.x = x
        self.y = y

        self.x2d, self.y2d = self._generate_2d_coords()

    def _generate_2d_coords(self):
        """Convert linear x and y into meshed x2d and y2d."""
        return np.meshgrid(self.x, self.y)

    def f(self, a):
        """Define function `f(x, y) = x + 3y - a` for single coefficient a."""
        return self.x2d + 3 * self.y2d - a

    def g(self, b):
        """Define function `g(x, y) = xy + b` for single coefficient b."""
        return self.x2d * self.y2d + b

    def h(self, c):
        """Define func `h(x, y) = 0.5x^2 + 3y + c` for single coefficient c."""
        return 0.5 * (self.x2d**2) + 3 * self.y2d + c

    def l(self, d):
        """Define func `l(x, y) = dx^2 + 2y + 4` for single coefficient d."""
        return d * (self.x2d**2) + 2 * self.y2d + 4

    def m(self, e):
        """Define func `m(x, y) = ey^2 + 0.5x^2 + e` for single coefficient e."""
        return e * (self.y2d ** 2) + 0.5 * (self.x2d ** 2) + e

    def combined(self, a, b, c, d, e):
        return self.f(a) + self.g(b) + self.h(c) + self.l(d) + self.m(e)


class Cell(object):
    def __init__(self):

        self.dim = '1' if self.y_extents is None else '2'

    def normalise(self, val, axis='i'):
        """
        Normalise the cell's extent into the interval (0, 1) as required by
        the reference cell.

        Cells can have any extents but the reference cell is defined within the
        interval (0, 1). This normalise ensures that this extent is always
        obeyed: input cells can have x-values of any interval and this method
        transforms the input x-values to the equivalent on the reference
        interval (0, 1).

        """
        def _norm(to_norm):
            return (to_norm - norm_arr[0]) / (norm_arr[-1] - norm_arr[0])

        # Setup.
        interval = [0, 1]
        if axis.lower() in ['i', 'x']:
            norm_arr = [self.i0, self.i1]
        elif axis.lower() in ['j', 'y'] and self.dim == '2':
            norm_arr = [self.j0, self.j1]
        else:
            msg = 'Invalid axis {!r} for a {}D cell.'
            raise ValueError(msg.format(axis, self.dim))

        # Input `val` needs to be a floating-point array.
        if not hasattr(val, '__iter__'):
            val = np.float(val)
            point = True
        else:
            val = val.astype(np.float)
            point = False

        # Perform normalisation if needed.
        if \
                (point and not interval[0] < val < interval[1]) or \
                (not point and not [val[0], val[1]] == interval):
            result = _norm(val)
        else:
            result = val
        return result

    @property
    def i0(self):
        return self.x_extents[0]

    @property
    def i1(self):
        return self.x_extents[1]

    @property
    def j0(self):
        return self.y_extents[0] if self.dim is '2' else None

    @property
    def j1(self):
        return self.y_extents[1] if self.dim is '2' else None


class Cell3DoF(Cell):
    def __init__(self, a, b, c, refcell, x_extents, y_extents=None):
        """
        Create a 1D or 2D cell with three coefficients (DoFs).

        :param a: DoF
        :param b: DoF
        :param c: DoF
        :param refcell: reference to the appropriate reference cell to apply
                        to this cell.
        :param x_extents: Extent of cell in x.
        :param y_extents: Extent of cell in y, if cell is 2D.

        """
        self.a = a
        self.b = b
        self.c = c
        self.refcell = refcell
        self.x_extents = sorted(x_extents)
        self.y_extents = sorted(y_extents) if y_extents is not None else None

        super(Cell3DoF, self).__init__()

    def __str__(self):
        cell_str = '{}D Cell(a={}, b={}, c={})'
        return cell_str.format(self.dim, self.a, self.b, self.c)

    def solve(self, x, y=None):
        """
        Apply a reference cell to a cell at the desired resolution as defined
        by `npoints`.

        The resolution can be a single value i.e. a solution at a point.

        """
        solutions = {}
        x_norm = self.normalise(x)
        if self.dim == 1:
            refcell = self.refcell(x_norm)
        else:
            y_norm = self.normalise(y, axis='j')
            refcell = self.refcell(x_norm, y_norm)
        solutions['f(x)'] = refcell.f(self.a)
        solutions['g(x)'] = refcell.g(self.b)
        solutions['h(x)'] = refcell.h(self.c)
        solutions['combined'] = refcell.combined(self.a, self.b, self.c)
        return solutions


class Cell5DoF(Cell):
    def __init__(self, a, b, c, d, e, refcell, x_extents, y_extents=None):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e
        self.refcell = refcell
        self.x_extents = sorted(x_extents)
        self.y_extents = sorted(y_extents) if y_extents is not None else None

        super(Cell5DoF, self).__init__()

    def __str__(self):
        c_str = '{}D Cell(a={}, b={}, c={}, d={}, e={})'
        return c_str.format(self.dim, self.a, self.b, self.c, self.d, self.e)

    def solve(self, x, y=None):
        """
        Apply a reference cell to a cell at the desired resolution as defined
        by `npoints`.

        The resolution can be a single value i.e. a solution at a point.

        """
        solutions = {}
        x_norm = self.normalise(x)
        if self.dim == 1:
            refcell = self.refcell(x_norm)
        else:
            y_norm = self.normalise(y, axis='j')
            refcell = self.refcell(x_norm, y_norm)
        solutions['f(x)'] = refcell.f(self.a)
        solutions['g(x)'] = refcell.g(self.b)
        solutions['h(x)'] = refcell.h(self.c)
        solutions['l(x)'] = refcell.h(self.d)
        solutions['m(x)'] = refcell.h(self.e)
        solutions['combined'] = refcell.combined(self.a, self.b, self.c,
                                                 self.d, self.e)
        return solutions
