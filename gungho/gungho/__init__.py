"""Gung-Ho! related Python library."""

import matplotlib.pyplot as plt
import numpy as np

import vec
import basis_func as bf


def load(dofs, dof_vert_mapping,
         vert_edge_mapping=None, vert_face_mapping=None):
    """
    Generate vec Verts and Cells from a list of DoFs.

    :param dofs: The DoFs.
    :param dof_vert_mapping: map DoF --> Vert.
    :param vert_edge_mapping: map 2 x Vert --> Edge.
    :param vert_face_mapping: map n x Vert --> Cell.

    :return: Lists of Verts and Cells.

    """
    # Create dict of verts.
    # verts = {k: vec.Vertex(v.inds,
    #                        data={'coeff': v.coefficient,
    #                              'x': v.x, 'y': v.y})
    #          for k, v in dof_vert_mapping.iteritems()}
    verts = {}
    for k, v in dof_vert_mapping.iteritems():
        index = v.inds
        vert_dict = v._asdict()
        vert_dict.pop('inds')
        verts[k] = vec.Vertex(index, data=vert_dict)

    # Create list of faces.
    faces = []
    for ind, emt in vert_face_mapping.iteritems():
        face_verts = [verts[i] for i in emt.inds]
        face = vec.Face(ind, face_verts, data=emt.coefficient)
        faces.append(face)

    return verts.values(), faces


def visualise(faces, key='combined'):
    """
    Use `basis_func` to visualise the result of applying basis functions to
    each loaded Cell.
    :param faces: loaded Cells.
    :param key: Basis function solution to plot.

    """
    # Setup.
    cell_extent_x = 1
    cell_extent_y = 1
    npoints = 10

    # Define axes.
    ax = plt.axes(aspect='equal')
    ax.margins(0.15)

    # Convert each face into a `CellxDoF` object and plot it.
    for face in faces:
        contents = face.contents()
        bl_vert_x = contents['verts'][0].x
        bl_vert_y = contents['verts'][0].y
        extent_x = [bl_vert_x, bl_vert_x+cell_extent_x]
        extent_y = [bl_vert_y, bl_vert_y+cell_extent_y]
        i = np.linspace(*extent_x, num=npoints)
        j = np.linspace(*extent_y, num=npoints)
        coeffs = [vert.coefficient for vert in contents['verts']]
        coeffs.append(face.data)
        cell_5f = bf.basis.Cell5DoF(*coeffs, refcell=bf.basis.RefCell2D5DoF,
                                    x_extents=extent_x, y_extents=extent_y)
        solutions = cell_5f.solve(i, j)
        bf.plot_2d(i, j, cell_5f, solutions, key=key, multiple=True)

    plt.show()
