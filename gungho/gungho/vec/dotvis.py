"""
Dot visualisations for vec.

"""

import pydot
from PIL import Image

import vec


class DotGraph(object):
    def __init__(self, cell, img_name, graph_type='digraph'):
        self.cell = cell
        self.img_name = img_name
        self.graph_type = graph_type

        # Create a graph to write to.
        self.graph = pydot.Dot(graph_type=self.graph_type)

    @staticmethod
    def _make_node(self, vert):
        """Create a pydot node from a vec Vertex."""
        return pydot.Node(vert.name())

    @staticmethod
    def _make_edge(self, edge):
        """Create a pydot edge from a vec Edge."""
        nodes = [self._make_node(v) for v in edge.verts()]
        return pydot.Edge(*nodes, edge.name())

    def graph_from_cell(self):
        verts = self.cell.contents()['verts']
        pass

    def save(self):
        self.graph.write_png(self.img_name)

    def show(self):
        graph_img = Image.open(self.img_name)
        graph_img.show()
