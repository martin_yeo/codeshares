STASH 'codeshares' repo
=======================

Code snippets for AVD use and sharing.

Usage:

  * Add whatever you like, update/merge your own contributions freely.
  * You probably want this on your python include path,
     - i.e. something like ``echo $(readlink -f .) >~/.local/lib/python2.7/site-packages/codeshares.pth``
  * But, please keep the namespace tidy!
