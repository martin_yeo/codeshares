"""
MOGREPS-G StaGE configuration.

"""
import numpy as np
import os
import os.path
from bgd.levels import HEIGHTS, PRESSURES
from bgd.plugins import *
from datetime import datetime as dt
from biggus import ConstantArray
from iris.coord_systems import GeogCS
from iris.coords import DimCoord
from iris.cube import Cube
from iris.fileformats.pp import EARTH_RADIUS


TARGET_PATH = os.environ.get('MOG_STAGE_INPUT_DIR',
                             '/critical/fpos2/cylc-run/mi-af228/share/data/'
                             '{}/engl_um_000/'.format(dt.now().strftime(
                                                      '%Y%m%dT0000Z')))

TIME_CHUNK = int(os.environ.get('STAGE_TIME_CHUNK', '6'))

INPUT_PATHS = {
    'englaa_pd': os.path.join(TARGET_PATH, 'englaa_pd{:03d}'.format(TIME_CHUNK)),
    'englaa_pf': os.path.join(TARGET_PATH, 'englaa_pf{:03d}'.format(TIME_CHUNK)),
}

OUTPUT_PATH = os.path.join(os.environ['DATADIR'], 'decoupler/mog_standard_u/')

ATTRIBUTES = {'grid_id': 'glm_standard'}


if not os.path.exists(OUTPUT_PATH):
    # Stop execution if the output directory doesn't exist. We could have done this
    # for the user, but we then need to be careful about race conditions for concurrent
    # execution.
    raise RuntimeError("Directory {} doesn't exist. Please create it.".format(OUTPUT_PATH))


def _make_global_grid(n_lat, min_lat, max_lat, n_lon, min_lon, max_lon,
                      bounds=True):
    """
    Create a two-dimensional Cube that represents the standard global grid.

    Returns
    -------
    Cube
        A global grid with the requested resolution.

    """
    cs = GeogCS(EARTH_RADIUS)
    lon_coord = iris.coords.DimCoord(np.linspace(min_lon, max_lon, n_lon),
                                     'longitude', units='degrees',
                                     coord_system=cs)
    lat_coord = iris.coords.DimCoord(np.linspace(min_lat, max_lat, n_lat),
                                     'latitude', units='degrees',
                                     coord_system=cs)
    if bounds:
        lon_coord.guess_bounds()
        lat_coord.guess_bounds()
    cube = iris.cube.Cube(ConstantArray((n_lat, n_lon)))
    cube.add_dim_coord(lat_coord, 0)
    cube.add_dim_coord(lon_coord, 1)
    return cube


# The standardised grid.
STANDARD_GRID = _make_global_grid(600, -89.85, 89.85,
                                  800, 0.225, 360 - 0.225)
# The model grid.
MODEL_GRID = _make_global_grid(600, -89.8499984741211, 89.85001373291016,
                               800, 0.22499999403953552, 359.7749938964844,
                               bounds=False)

#MODEL_OROGRAPHY = FirstTimeSlice(Load('orog', 'm01s00i033'))
#STANDARD_OROGRAPHY = RegridBilinear(MODEL_OROGRAPHY, STANDARD_GRID)

# A dictionary mapping name --> plugin, for all the "Level 1.0" outputs.

LEVEL1 = {
    '01_16004_0': EnsureDimension(
                      AddMissingRealization(
                          RegridBilinear(
                              HybridHeightToTerrainFollowing(
                                  StructuredFFLoad('englaa_pf',
                                      'm01s16i004'),
                                  HEIGHTS),
                              STANDARD_GRID),
                          ),
                      'realization'),
                  }


# CDI has very specific requirements. We attach those to the processsed data.
LEVEL1 = {name: EnsureDimension(
                    SetAttributes(
                        ConvertTimeToSeconds(
                            ConvertModelLevelNumber(config)
                        ),
                        ATTRIBUTES
                    ),
                    'time'
                )
          for name, config in LEVEL1.items()}


# A dictionary mapping name --> plugin, for all the "Level 2.0" outputs.
LEVEL2 = {}


# A dictionary mapping name --> plugin, for all the "Level 3.0" outputs.
LEVEL3 = {}

