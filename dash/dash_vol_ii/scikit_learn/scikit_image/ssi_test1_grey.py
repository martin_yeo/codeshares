'''
example using skimage SSI on some AVD plot data

greyscale version

'''

import numpy as np
import matplotlib.pyplot as plt

from skimage import data, img_as_float
from skimage.measure import compare_ssim as ssim

data_path = './'

known_good_img = data.imread(data_path + 'good_example.png', as_grey=True)
bad_img = data.imread(data_path + 'bad_example.png', as_grey=True)
test_img = data.imread(data_path + 'candidate.png', as_grey=True)


def mse(x, y):
    return np.linalg.norm(x - y)

# Compare known good with bad

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10),
                         sharex=True, sharey=True,
                         subplot_kw={'adjustable': 'box-forced'})
ax = axes.ravel()

mse_none = mse(known_good_img, known_good_img)
ssim_none = ssim(known_good_img, known_good_img,
                  data_range=known_good_img.max() - known_good_img.min())

mse_bad = mse(known_good_img, bad_img)
ssim_bad = ssim(known_good_img, bad_img,
                  data_range=bad_img.max() - bad_img.min())


label = 'MSE: {:.2f}, SSIM: {:.2f}'

ax[0].imshow(known_good_img, cmap=plt.cm.gray, vmin=0, vmax=1)
ax[0].set_xlabel(label.format(mse_none, ssim_none))
ax[0].set_title('Original image')

ax[1].imshow(bad_img, cmap=plt.cm.gray, vmin=0, vmax=1)
ax[1].set_xlabel(label.format(mse_bad, ssim_bad))
ax[1].set_title('Good vs Bad')


plt.tight_layout()
plt.show()

# Compare known good with candidate

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10),
                         sharex=True, sharey=True,
                         subplot_kw={'adjustable': 'box-forced'})
ax = axes.ravel()

mse_none = mse(known_good_img, known_good_img)
ssim_none = ssim(known_good_img, known_good_img,
                  data_range=known_good_img.max() - known_good_img.min())

mse_bad = mse(known_good_img, test_img)
ssim_bad = ssim(known_good_img, test_img,
                  data_range=test_img.max() - test_img.min())


label = 'MSE: {:.2f}, SSIM: {:.2f}'

ax[0].imshow(known_good_img, cmap=plt.cm.gray, vmin=0, vmax=1)
ax[0].set_xlabel(label.format(mse_none, ssim_none))
ax[0].set_title('Original image')

ax[1].imshow(test_img, cmap=plt.cm.gray, vmin=0, vmax=1)
ax[1].set_xlabel(label.format(mse_bad, ssim_bad))
ax[1].set_title('Good vs Test')


plt.tight_layout()
plt.show()
