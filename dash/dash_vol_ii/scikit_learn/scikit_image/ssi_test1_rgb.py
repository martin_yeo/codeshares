'''
example using skimage SSI on some AVD plot data

rgb example
'''

import numpy as np
import matplotlib.pyplot as plt

from skimage import color, data, img_as_float
from skimage.measure import compare_ssim as ssim

data_path = './'

known_good_img = data.imread(data_path + 'good_example.png')
bad_img = data.imread(data_path + 'bad_example.png')
test_img = data.imread(data_path + 'candidate.png')


def mse(x, y):
    return np.linalg.norm(x - y)

# Compare known good with a bad image

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10),
                         sharex=True, sharey=True,
                         subplot_kw={'adjustable': 'box-forced'})
ax = axes.ravel()

# Compute metrics for r,g,b channels separately and average

mse_none_r = mse(known_good_img[...,0], known_good_img[...,0])
mse_none_g = mse(known_good_img[...,1], known_good_img[...,1])
mse_none_b = mse(known_good_img[...,2], known_good_img[...,2])

mse_none = (mse_none_r + mse_none_g + mse_none_b) / 3.0

ssim_none_r = ssim(known_good_img[...,0], known_good_img[...,0],
                  data_range=known_good_img[...,0].max() - known_good_img[...,0].min())
ssim_none_g = ssim(known_good_img[...,1], known_good_img[...,1],
                  data_range=known_good_img[...,1].max() - known_good_img[...,1].min())
ssim_none_b = ssim(known_good_img[...,2], known_good_img[...,2],
                  data_range=known_good_img[...,2].max() - known_good_img[...,2].min())

ssim_none = (ssim_none_r + ssim_none_g + ssim_none_b) / 3.0


mse_bad_r = mse(known_good_img[...,0], bad_img[...,0])
mse_bad_g = mse(known_good_img[...,1], bad_img[...,1])
mse_bad_b = mse(known_good_img[...,2], bad_img[...,2])

mse_bad = (mse_bad_r + mse_bad_g + mse_bad_b) / 3.0


ssim_bad_r = ssim(known_good_img[...,0], bad_img[...,0],
                  data_range=bad_img[...,0].max() - bad_img[...,0].min())
ssim_bad_g = ssim(known_good_img[...,1], bad_img[...,1],
                  data_range=bad_img[...,1].max() - bad_img[...,1].min())
ssim_bad_b = ssim(known_good_img[...,2], bad_img[...,2],
                  data_range=bad_img[...,2].max() - bad_img[...,2].min())

ssim_bad = (ssim_bad_r + ssim_bad_g + ssim_bad_b) / 3.0

print "Bad", ssim_bad_r, ssim_bad_g, ssim_bad_b

label = 'MSE: {:.2f}, SSIM: {:.2f}'

ax[0].imshow(known_good_img)
ax[0].set_xlabel(label.format(mse_none, ssim_none))
ax[0].set_title('Original image')

ax[1].imshow(bad_img)
ax[1].set_xlabel(label.format(mse_bad, ssim_bad))
ax[1].set_title('Known Good vs Bad')


plt.tight_layout()
plt.show()

# Compare known good with test image

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 10),
                         sharex=True, sharey=True,
                         subplot_kw={'adjustable': 'box-forced'})
ax = axes.ravel()

# Compute metrics for r,g,b channels separately and average

mse_none_r = mse(known_good_img[...,0], known_good_img[...,0])
mse_none_g = mse(known_good_img[...,1], known_good_img[...,1])
mse_none_b = mse(known_good_img[...,2], known_good_img[...,2])

mse_none = (mse_none_r + mse_none_g + mse_none_b) / 3.0

ssim_none_r = ssim(known_good_img[...,0], known_good_img[...,0],
                  data_range=known_good_img[...,0].max() - known_good_img[...,0].min())
ssim_none_g = ssim(known_good_img[...,1], known_good_img[...,1],
                  data_range=known_good_img[...,1].max() - known_good_img[...,1].min())
ssim_none_b = ssim(known_good_img[...,2], known_good_img[...,2],
                  data_range=known_good_img[...,2].max() - known_good_img[...,2].min())

ssim_none = (ssim_none_r + ssim_none_g + ssim_none_b) / 3.0

mse_test_r = mse(known_good_img[...,0], test_img[...,0])
mse_test_g = mse(known_good_img[...,1], test_img[...,1])
mse_test_b = mse(known_good_img[...,2], test_img[...,2])

mse_test = (mse_test_r + mse_test_g + mse_test_b) / 3.0


ssim_test_r = ssim(known_good_img[...,0], test_img[...,0],
                  data_range=test_img[...,0].max() - test_img[...,0].min())
ssim_test_g = ssim(known_good_img[...,1], bad_img[...,1],
                  data_range=test_img[...,1].max() - test_img[...,1].min())
ssim_test_b = ssim(known_good_img[...,2], bad_img[...,2],
                  data_range=test_img[...,2].max() - test_img[...,2].min())

ssim_test = (ssim_test_r + ssim_test_g + ssim_test_b) / 3.0

print "Test", ssim_test_r, ssim_test_g, ssim_test_b

label = 'MSE: {:.2f}, SSIM: {:.2f}'

ax[0].imshow(known_good_img)
ax[0].set_xlabel(label.format(mse_none, ssim_none))
ax[0].set_title('Original image')

ax[1].imshow(test_img)
ax[1].set_xlabel(label.format(mse_test, ssim_test))
ax[1].set_title('Known Good vs test image')


plt.tight_layout()
plt.show()

