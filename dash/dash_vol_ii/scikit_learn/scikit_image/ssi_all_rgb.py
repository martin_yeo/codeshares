'''
example using skimage SSI on all the AVD test plot data (rgb version)

this code trawls through a directory of images, tries to find

a corresponding known good image then computes the MSE and SSI

and writes to stdout in a csv format
'''

import numpy as np
import matplotlib.pyplot as plt

from skimage import data, img_as_float
from skimage.measure import compare_ssim as ssim

import glob

def mse(x, y):
    return np.linalg.norm(x - y)

goodImagePath = '../good/known_good/'

joiner = '.'


# Process bad images

dataPath = '../bad/'

# get a list of directories 
badDirList = glob.glob(dataPath + '*')


for d in badDirList:

   if d != '../good/known_good':

     print "Directory", d
     fileList = glob.glob(dataPath + d + '/*')
   
     for f in fileList:
        # try and find the known good image for this image
        searchStr = '*' + joiner.join(f.split('.')[-4:-1]) + '*'
        #print dataPath + d + '/' + searchStr
        kg = glob.glob(goodImagePath + searchStr)
        #print f
        #print kg[0]
        # ignore any where there are multiple or no matches
        if len(kg) == 1:
          known_good_img = data.imread(kg[0])
          test_img = data.imread(f)

          mse_test_r = mse(known_good_img[...,0], test_img[...,0])
          mse_test_g = mse(known_good_img[...,1], test_img[...,1])
          mse_test_b = mse(known_good_img[...,2], test_img[...,2])

          mse_test = (mse_test_r + mse_test_g + mse_test_b) / 3.0

          ssim_test_r = ssim(known_good_img[...,0], test_img[...,0],
                  data_range=test_img[...,0].max() - test_img[...,0].min())
          ssim_test_g = ssim(known_good_img[...,1], test_img[...,1],
                  data_range=test_img[...,1].max() - test_img[...,1].min())
          ssim_test_b = ssim(known_good_img[...,2], test_img[...,2],
                  data_range=test_img[...,2].max() - test_img[...,2].min())

          ssim_test = (ssim_test_r + ssim_test_g + ssim_test_b) / 3.0
          print searchStr + "," + str(mse_test) + ',' + str(ssim_test) 

