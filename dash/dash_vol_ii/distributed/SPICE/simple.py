import sys

from distributed import Client


def inc(x):
    return x + 1


host = sys.argv[1]
print("Client connecting to scheduler @ {}".format(host))
client = Client(host)

#x = client.submit(inc, 10)

y = client.map(inc, range(10000))
y

