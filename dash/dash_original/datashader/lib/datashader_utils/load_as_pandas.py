"""SA data as a pandas dataframe."""

import os

import iris
import pandas as pd


def obscubes_to_pandas_df(fp, fn, name):
    cubes = iris.load(os.path.join(fp, fn))
    channel = 38
    df = pd.DataFrame({'data': cubes.extract(name)[0].data[channel],
                       'latitude': cubes.extract('latitude')[0].data,
                       'longitude': cubes.extract('longitude')[0].data,
                       'time': cubes.extract('time')[0].data,
                       'satid': cubes.extract('SatId')[0].data,
                       })
    return df


def cube_to_pandas_df(fp, fn, name):
    cube = iris.load_cube(os.path.join(fp, fn), name)
    channel = 38
    df = pd.DataFrame({'data': cube.data[channel],
                       'latitude': cube.coord('latitude').points,
                       'longitude': cube.coord('longitude').points,
                       'time': cube.coord('time').points,
                       'satid': cube.coord('SatId').points,
                       })
    return df


def main():
    fp = '/data/users/dkillick/data/sa_avd'
    fn = 'ATOVS_many_201605081200_v2.nc'
    fn2 = 'ATOVS_Write201607041200_pe0.nc'
    # dataframe = cube_to_pandas_df(fp, fn, 'raw brightness temp')
    dataframe = obscubes_to_pandas_df(fp, fn2, 'BriTemp_raw')
    # dataframe.dropna(how='any')
    print dataframe.describe()
    df = dataframe[dataframe.data > 0]
    print df.describe()
    # print dataframe[dataframe.satid == 3].describe()


if __name__ == '__main__':
    main()
