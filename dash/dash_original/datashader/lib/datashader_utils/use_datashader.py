"""Trialling using datashader for plotting satellite data."""

from functools import partial
import os

import iris
import matplotlib.pyplot as plt
import pandas as pd

from mpl_wrapper import LiveImageDisplay


# See https://github.com/bokeh/datashader/blob/master/examples/census.ipynb
# background = "black"
# cm = partial(colormap_select, reverse=(background!="black"))


def obscubes_to_pandas_df(fp, fn, name):
    cubes = iris.load(os.path.join(fp, fn))
    channel = 38
    df = pd.DataFrame({'data': cubes.extract(name)[0].data[channel],
                       'latitude': cubes.extract('latitude')[0].data,
                       'longitude': cubes.extract('longitude')[0].data,
                       'time': cubes.extract('time')[0].data,
                       'satid': cubes.extract('SatId')[0].data,
                       })
    df['cat_satid'] = df['satid'].astype('category')
    return df


def cube_to_pandas_df(fp, fn, name):
    cube = iris.load_cube(os.path.join(fp, fn), name)
    channel = 38
    df = pd.DataFrame({'data': cube.data[channel],
                       'latitude': cube.coord('latitude').points,
                       'longitude': cube.coord('longitude').points,
                       'time': cube.coord('time').points,
                       'satid': cube.coord('SatId').points,
                       })
    df['cat_satid'] = df['satid'].astype('category')
    return df


def plot(dataframe, lat, lon, reduction, col_red):
    # Variables.
    xmin = -180.0
    ymin = -90.0
    xmax = 180.0
    ymax = 90.0

    img = LiveImageDisplay(dataframe, lat, lon, 500, 500,
                           agg_reduction=reduction,
                           agg_reduction_col=col_red)
    z = img(xmin, xmax, ymin, ymax)
    # res = z.to_masked_array()
    # out = np.ma.masked_equal(res, 0)

    vir_cm = plt.cm.viridis
    vir_cm.set_under('w')
    vir_cm.set_bad('w')

    fig1, ax2 = plt.subplots(1, 1)
    m = ax2.imshow(z, origin='lower', extent=(xmin, xmax, ymin, ymax),
                   cmap=vir_cm, vmin=0.1)
    plt.colorbar(m)

    # Connect for changing the view limits
    ax2.callbacks.connect('xlim_changed', img.ax_update)
    ax2.callbacks.connect('ylim_changed', img.ax_update)

    plt.show()


def main():
    fp = '/data/users/dkillick/data/sa_avd'
    fn = 'ATOVS_many_201605081200_v2.nc'
    fn2 = 'ATOVS_Write201607041200_pe0.nc'
    dataframe = cube_to_pandas_df(fp, fn, 'raw brightness temp')
    # dataframe = obscubes_to_pandas_df(fp, fn2, 'BriTemp_raw')
    df = dataframe[dataframe.data > 0]
    # df = df[df.satid == 3]
    print df.describe()
    plot(df, 'longitude', 'latitude', 'mean', 'data')


if __name__ == '__main__':
    main()
