import metdb
import pickle

data = metdb.obs('joel.dyer@metoffice.gov.uk', 'ATDNET',
                 keywords=['START TIME 20160724/1200Z'],
                 elements=['LNGD', 'LTTD'])

file = open('LDat201607241200Z', 'w')

pickle.dump(data, file)