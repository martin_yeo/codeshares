#!/bin/bash -u
#
# Regrid cubed sphere data to latlon grid:  "realistic data demonstration" 
# -----------------------------------------------------------------------------
#
# The following script demonstrates regridding sample LFRic "surface_altitude" data onto
# the UM (~10km) grid.  The raw and regridded results are plotted.
#
# The (~high) resolution of the UM (target) grid warrants executing the processing on SPICE.
# Therefore, run the script using:   sbatch /full/path/to/spice_run_regrid.sh
#
# See the regridding script for further information, including caveats.
#

#SBATCH --mem-per-cpu=20G
#SBATCH --output=spice_run_output.txt
#SBATCH --time=0:20:00

set -eu

# Ensure running via SPICE - data is big!
if [[ $(tty) =~ "not a tty" ]]; then
    echo "Running job on SPICE"
else
    echo -e "Script should be run on SPICE via:\n  sbatch $0 \nAborting." 1>&2
    exit 1
fi

# The NGVAT custom (Python) environment
ENV_NAME=ngvat_regrid_demo_env
ENV_PATH=/project/avd/ng-vat/environments/${ENV_NAME}

# Regridding script:
REGRID_PYSCRIPT=${ENV_PATH}/bin/regrid_xios_to_um.py
# Args:
lfric_file="/project/avd/ng-vat/data/sprint_0c_20200205/data_used_in_google_earth_demo/qrparm.orog.ugrid.nc"
lfric_diagnostic="surface_altitude"
um_file="/project/avd/ng-vat/data/sprint_0d_20200219/umglaa_pb000-theta.pp"
um_diagnostic="air_potential_temperature"
plotname="${PWD}/plots_regrid_lfric_to_um__${lfric_diagnostic}.png"

# Create output directory
mkdir -p $(dirname ${plotname})

echo "Running regridding"
${ENV_PATH}/bin/python ${REGRID_PYSCRIPT} --lfric_data ${lfric_file} ${lfric_diagnostic} --grid ${um_file} ${um_diagnostic} --plot_filename ${plotname}

echo "Finished running: $0"
