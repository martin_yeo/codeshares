# Test the custom environment we provide for trials of iris-ugrid regridding.

import os
from platform import python_version
from pytest import main as pytest_main

expected_iris_version = '3.0.0ugrid1'
expected_irisugrid_version = '0.1.0regrid1'

DEMO_ENV_PATH=os.environ['DEMO_ENV_PATH']

msg_start = (f"Testing regrid demo env at '{DEMO_ENV_PATH}' :"
             "\n  - can import 'iris' + 'iris-ugrid'."
             "\n  - package paths + versions as expected."
             "\n  - iris-ugrid's tests pass."
             "\n  - the demo script is present.")

msg_ok = f"\nTested regrid env at '{DEMO_ENV_PATH}'.\nAll OK."

msg_fail = f"\n\nProblem testing env at {DEMO_ENV_PATH}.\nTest failed."

import_msg = (
    "\nError:\n{errmsg}\n\nFailed to import package '{pkg}'." +
    msg_fail)

path_msg = (
    "\nExpected env path '{env_path}' not found in {pkg}.__file__ "
    "\n  : {pkg}.__file__ = '{pkg_path}'."
    "\nRemove import overrides in PYTHONPATH or user site-packages"
    " extension (~/.local/lib/pythonX.Y-site-packages) ?") + msg_fail

vsn_msg = (
    "\nUnexpected {pkg} package version, '{pkg}.__version__' :"
    "\n  expected '{expected}'"
    "\n  but got '{actual}'.") + msg_fail

def error(msg):
    print(msg)
    exit(1)


print(msg_start)

try:
    import iris
except ImportError as err:
    error(import_msg.format(errmsg=str(err), pkg='iris'))

if DEMO_ENV_PATH not in iris.__file__:
    error(path_msg.format(
        pkg='iris', env_path=DEMO_ENV_PATH, pkg_path=iris.__file__))

actual_iris_version = iris.__version__
if actual_iris_version != expected_iris_version:
    error(vsn_msg.format(
        pkg='iris',
        expected=expected_iris_version,
        actual=actual_iris_version))

try:
    import iris_ugrid
except ImportError as err:
    error(import_msg.format(errmsg=str(err), pkg='iris_ugrid'))

if DEMO_ENV_PATH not in iris_ugrid.__file__:
    error(path_msg.format(
        pkg='iris_ugrid', env_path=DEMO_ENV_PATH,
        pkg_path=iris_ugrid.__file__))

actual_irisugrid_version = iris_ugrid.__version__
if actual_irisugrid_version != expected_irisugrid_version:
    error(vsn_msg.format(
        pkg='iris_ugrid',
        expected=expected_irisugrid_version,
        actual=actual_irisugrid_version))

print("RUNNING IRIS-UGRID tests...")
py_version = ".".join(python_version().split(".")[:2])
ugrid_install_path = os.path.join(DEMO_ENV_PATH, "lib",
                                  f"python{py_version}", "site-packages",
                                  "iris_ugrid")
if pytest_main([ugrid_install_path]) != 0:
    error("pytest error - see output.")


env_scripts_dir = os.path.join(DEMO_ENV_PATH, "bin")

demo_script_path = os.path.join(env_scripts_dir, "regrid_xios_to_um.py")
if not os.path.exists(demo_script_path):
    error(f"Demo regrid script, file not found : {demo_script_path}.")

spice_script_path = os.path.join(env_scripts_dir, "spice_run_regrid.sh")
if not os.path.exists(spice_script_path):
    error(f"Slurm test script, file not found : {spice_script_path}.")

print(msg_ok)
