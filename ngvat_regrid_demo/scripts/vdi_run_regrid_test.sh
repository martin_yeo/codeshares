#!/bin/bash
#
# Regrid cubed sphere data to latlon grid:  "small test"
# -----------------------------------------------------------------------------
#
# The following script demonstrates regridding sample LFRic "surface_altitude" data onto
# a small (160x80 grid point) UM-like grid, a grid with comparable resolution to the
# LFRic data (approx 13,000 cells).  The raw and regridded results are plotted.
#
# This quick demo can be run by:  /full/path/to/vdi_run_regrid_test.sh
#
# See the regridding script for further information, including caveats.
# We recommend using grids/meshes no larger than 2,000,000 cells to avoid memory problems.
#

set -eu

# The NGVAT custom (Python) environment
ENV_NAME=ngvat_regrid_demo_env
ENV_PATH=/project/avd/ng-vat/environments/${ENV_NAME}

# Regridding script:
REGRID_PYSCRIPT=${ENV_PATH}/bin/regrid_xios_to_um.py
# Args:
lfric_file="/project/avd/ng-vat/data/sprint_0c_20200205/data_used_in_google_earth_demo/qrparm.orog.ugrid.nc"
lfric_diagnostic="surface_altitude"
grid_specification="160 80 -180 180 -90 90"
plotname="${PWD}/plots_regrid_lfric_to_um__160x80_test.png"

# Create output directory
mkdir -p $(dirname ${plotname})

echo "Running small test data"
${ENV_PATH}/bin/python ${REGRID_PYSCRIPT} --lfric_data ${lfric_file} ${lfric_diagnostic} --grid ${grid_specification} --plot_filename ${plotname}

echo "Finished running: $0"
