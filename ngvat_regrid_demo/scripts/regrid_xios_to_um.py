#!/project/avd/ng-vat/environments/ngvat_regrid_demo_env/bin/python
"""
(AW) Regridding LFRic cubed-sphere data to UM-esque rectilinear grid
--------------------------------------------------------------------

Introduction
------------
This script utilizes the (new) `iris_ugrid.regrid` module for
regridding data output on an unstructured mesh, to a regular lat-lon
grid. The data is regridded using first-order conservative regridding.

Helper functions are provided for:
  - reading (the required information) from an LFRic (XIOS) diagnostic
  - deriving a UM-like grid (onto which the diagnostic will be regridded)
  - plotting the raw and regridded results.

The regridding itself is achieved by calling `iris_ugrid.regrid` to:
 1) instantiate the regridder
    (calculates regridding weights based on the geometry of the underlying grids)
 2) perform the regridding
    (interpolates the data from the source mesh, outputting it on the target grid)

A note:
-------
To date, the `iris_ugrid.regrid` interfaces with the
Earth System Modelling Framework (ESMF), which regrids the unstructured
data.  The `iris_ugrid.regrid` is planned to be accessible via `iris`
in the future.


Running the script:
-------------------
For help running the script:   ./regrid_xios_to_um.py -h

See the accompanying wrapper scripts for a demonstration of how
sample orography ("surface_altitude") data is extracted from a test
LFRic output (XIOS) file, and regridded on a (small) global UM-like
lon-lat grid and plotted (to a file), and for how the target grid
can be derived from existing UM data.

Timing information is written at each step, for reference.

It is hoped that the script demonstrates how LFRic data can be
regridded on to a UM like grid, using the new facilities, so that users
can incorporate the functionality into their own code or adapt the
script to meet their current needs.

"""

from copy import deepcopy
from datetime import datetime, timedelta
import sys
import argparse
import textwrap
from contextlib import contextmanager

import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.collections import PolyCollection

from netCDF4 import Dataset
import numpy as np

from cartopy.crs import PlateCarree
import iris
import iris.analysis.cartography
from iris.cube import Cube
from iris.coord_systems import GeogCS
from iris.coords import DimCoord
from iris.fileformats.pp import EARTH_RADIUS

from iris_ugrid import regrid


def _get_parser():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent("""\
            (AW) Regrid LFRic cubed-sphere data to UM-esque rectilinear grid
            --------------------------------------------------------------------
            """),
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=textwrap.dedent("""\
            Caveats:  There are a couple of caveats wrt the current code:
             - The regridding scheme is restricted to area-weighted interpolation.
             - Only face-centred cubed sphere data can be correctly processed.
             - Only global (non-rotated) cylindrical lat-lon grids have be tested as the target grid.

            Please see the script docstring for further information regarding intended useage.
            
            """),
    )

    # LFRic data
    parser.add_argument(
        "-lf", "--lfric_data",
        nargs='+', type=str, required=True,
        help=textwrap.dedent("""\
             The LFRic (XIOS) data to be regridded.
             Provide the LFRic file path and diagnostic name as space separated strings akin to:
              '/full/path/to/the/lfric.nc surface_altitude'.
            
            """),
    )

    # UM grid information
    parser.add_argument(
        "-g", "--grid",
        nargs='+', type=str, required=True,
        help=textwrap.dedent("""\
            The information for deriving the grid that the data will be regridded onto.
            It is noted that Level 0 UM diagnostics are output on a variety of Arawaka grids,
            are are often regridded to the Level 1 "standard grid".
            Hence, different target grids are likely to be required according to the
            diagnostic being analysed and the type of evaluation being undertaken.
            Therefore, it is desirable to derive the target grid either by:
                a) reading it from an exiting UM diagnostic.
                   - by providing the UM file path and a diagnostic akin to:
                   '/path/to/um_file um_diagnostic_name' (the UM diagnostic name can be a STASH code), 
                   e.g.
                   '/project/avd/ng-vat/data/sprint_0d_20200219/umglaa_pb000-theta.pp air_potential_temperature'
            
             or b) by specifying the grid.
                   - by listing the number of grid points and the grid bounds akin to:
                   'n_lons, n_lats, western, eastern, southern, northern' bounds
                    e.g. '160 80 -180 180 -90 90'
            
            """),
    )

    # Set plot file name
    parser.add_argument(
        "-p", "--plot_filename",
	type=str,
	help="The full path and name of the output plot, e.g. ./plots_regrid_xios_to_um.png",
        default='./plots_regrid_xios_to_um.png'
    )

    return parser


@contextmanager
def report_time(times):
    """
    maintain state within a simple context manager
    """
    start = datetime.now()
    try:
        yield
    finally:
        time_value = datetime.now() - start
        times.append(time_value)
        print(f"Time elapsed: {time_value}\n")


def load_lfric_data(lfric_path, diagnostic):
    with Dataset(lfric_path) as lfric_dataset:
        if "dynamics_face_nodes" not in lfric_dataset.variables.keys():
            msg = ("Can not read mesh information.  Currently the face-node"
                   " connectivity information is obtained \nfrom variables: "
                   "dynamics_face_nodes, dynamics_node_x, dynamics_node_y \n"
                   "but they were are not found in the data. Aborting.")
            raise ValueError(msg)
        face_nodes = lfric_dataset["dynamics_face_nodes"][:]
        node_start_index = lfric_dataset["dynamics_face_nodes"].start_index
        node_xs = lfric_dataset["dynamics_node_x"][:]  # typically in degrees_east
        node_ys = lfric_dataset["dynamics_node_y"][:]  # typically in degrees_north
        node_coords = np.stack((node_xs, node_ys), axis=1)
        lfric_data = lfric_dataset[diagnostic][:]
    return node_coords, face_nodes, node_start_index, lfric_data


def _generate_bounds(coord_array):
    diffs = np.diff(coord_array)
    diffs = np.insert(diffs, 0, diffs[0])
    diffs = np.append(diffs, diffs[-1])
    bounds = coord_array - diffs[:-1] * 0.5
    final_bound = coord_array[-1] + diffs[-1] * 0.5
    bounds = np.append(bounds, final_bound)
    return bounds


def load_um_grid(um_path, diagnostic=None):
    cube = iris.load_cube(um_path, diagnostic)
    longitudes = cube.coord("longitude").points
    latitudes = cube.coord("latitude").points
    lon_bounds = _generate_bounds(longitudes)
    lat_bounds = _generate_bounds(latitudes)
    return longitudes, latitudes, lon_bounds, lat_bounds


def create_um_grid(n_lon, min_lon, max_lon, n_lat, min_lat, max_lat):
    """
    Create longitude and latitude grid information akin to the global UM
    rectilinear grid.
    n_lon and n_lat are the numbers of grid points in the E-W and N-S
    directions respectively.
    The max and min values refer to the extents the grid bounds (not the
    values of the grid points themselves)
    """
    step = (max_lon - min_lon) / (n_lon)
    longitudes = np.linspace(min_lon + 0.5 * step, max_lon - 0.5 * step, n_lon)
    step = (max_lat - min_lat) / (n_lat)
    latitudes = np.linspace(min_lat + 0.5 * step, max_lat - 0.5 * step, n_lat)
    lon_bounds = _generate_bounds(longitudes)
    lat_bounds = _generate_bounds(latitudes)
    return longitudes, latitudes, lon_bounds, lat_bounds


def plot_raw_and_regridded_results(node_coords, face_nodes, node_start_index, lfric_data,
                                   regridded_result, longitudes, latitudes, lon_bounds, lat_bounds, plot_file):
    def cubed_sphere_fix(vertices):
        """
        Credit Paul Earnshaw
        https://www.yammer.com/metoffice.gov.uk/#/Threads/show?threadId=424429100425216
        """

        n_face, n_npf, _ = vertices.shape
        for i in range(n_face):
            for j in range(n_npf):
                # If north or south pole then it needs an appropriate
                #  "drawing" longitude
                if vertices[i, j, 1] in (-90, 90):
                    vertices[i, j, 0] = vertices[i, j - 2, 0]
                # If zero meridian and other points are on west side, then set
                #  to 360 instead of 0
                if vertices[i, j, 0] == 0.0:
                    if any(vertices[i, :, 0] > 180):
                        vertices[i, j, 0] = 360.0
        return vertices

    face_coords = node_coords[face_nodes - node_start_index]
    face_coords = cubed_sphere_fix(face_coords)
    coll = PolyCollection(face_coords,
                          array=lfric_data,
                          transform=PlateCarree(),
                          edgecolors="face")

    fig = plt.figure(figsize=(10, 10))
    grid_spec = fig.add_gridspec(2, 3)

    def global_plot(row, data_array, area_weights=None):
        meanval = np.average(data_array, weights=area_weights)
        diffs = data_array - meanval
        stdval = np.sqrt(np.average(diffs * diffs, weights=area_weights))
        metrics_dict = {
            "min": np.min(data_array),
            "max": np.max(data_array),
            "mean": meanval,
            "st-dev": stdval,
            "count": np.size(data_array)
        }
        metrics_string = "\n".join([f"{metric}: {round(value, 2)}" for
                                    metric, value in metrics_dict.items()])

        new_plot = fig.add_subplot(grid_spec[row, :2], projection=PlateCarree())
        new_plot.set_global()
        new_plot.text(-175, -50, metrics_string, color="white")
        new_plot.coastlines()
        return new_plot

    def zoomed_plot(row):
        new_plot = fig.add_subplot(grid_spec[row, 2], projection=PlateCarree())
        new_plot.set_title("<-- zoomed (McMurdo Sound)")
        new_plot.set_extent([163.7, 164.7, -75.9, -76.9])
        return new_plot

    lfric_global = global_plot(row=0, data_array=lfric_data)
    lfric_global.set_title("LFRic cubesphere data")
    lfric_zoom = zoomed_plot(row=0)
    for subplot in (lfric_global, lfric_zoom):
        subplot.add_collection(deepcopy(coll))

    # Calculate cell areas for the UM latlon grid
    # Create a dummy grid cube so we can use Iris
    um_cube = Cube(np.zeros((len(latitudes), len(longitudes))))

    def coord_bounds_from_contiguous(bounds):
        n_pts = len(bounds) - 1
        coord_bounds = np.zeros((n_pts, 2))
        coord_bounds[..., 0] = bounds[:-1]
        coord_bounds[..., 1] = bounds[1:]
        return coord_bounds

    um_cube.add_dim_coord(
        DimCoord(latitudes, bounds=coord_bounds_from_contiguous(lat_bounds),
                 standard_name='latitude', units='degrees',
                 coord_system=GeogCS(EARTH_RADIUS)),
        0)

    um_cube.add_dim_coord(
        DimCoord(longitudes, bounds=coord_bounds_from_contiguous(lon_bounds),
                 standard_name='longitude', units='degrees',
                 coord_system=GeogCS(EARTH_RADIUS)),  # this just avoids a warning
        1)

    # Use Iris to calculate the cell areas. Note we are transposing the weights so the array is
    # in the same order as the regridded_result (which has inherited the order from ESMF)
    um_area_weights = np.transpose(iris.analysis.cartography.area_weights(um_cube, normalize=True))

    um_global = global_plot(row=1, data_array=regridded_result,
                            area_weights=um_area_weights)
    um_global.set_title("LFRic cubesphere data regridded to UM lon-lat grid")
    um_zoom = zoomed_plot(row=1)
    for subplot in (um_global, um_zoom):
        subplot.pcolormesh(lon_bounds, lat_bounds, np.transpose(regridded_result))

    plt.savefig(plot_file)


if __name__ == '__main__':

    times_list = []

    # INPUTS ######################################################################

    parser = _get_parser()
    args = parser.parse_args()

    lfric_path, lfric_diagnostic = args.lfric_data
    grid_specification = args.grid
    plotfile_path = args.plot_filename


    # LFRIC #######################################################################

    print("Loading LFRic data ...")
    with report_time(times_list):
        node_coords, face_nodes, node_start_index, lfric_data = load_lfric_data(lfric_path, lfric_diagnostic)


    # UM ##########################################################################

    print("Getting latlon grid")
    with report_time(times_list):
        if len(grid_specification) == 2:
            print("Derive grid from an existing UM diagnostic.  Loading UM grid ...")
            um_path, um_diagnostic = grid_specification
            longitudes, latitudes, lon_bounds, lat_bounds = load_um_grid(um_path, um_diagnostic)

        elif len(grid_specification) == 6:
            print("Creating UM-esque rectilinear grid ...")
            grid_specification = list(map(int, grid_specification))
            n_lons, n_lats, western, eastern, southern, northern = grid_specification
            longitudes, latitudes, lon_bounds, lat_bounds = (
                create_um_grid(n_lons, western, eastern, n_lats, southern, northern)
            )

        else:
            print("Target grid specifications are incorrect. Aborting")
            sys.exit(1)


    # REGRID ######################################################################

    print("Regridding LFRic data using first-order conservative regridding... ")
    with report_time(times_list):
        # MeshInfo: LFRic data is stored on a cubesphere - described using
        # an irregular Mesh.
        print("Instantiating MeshInfo ...")
    
        mesh_info = regrid.MeshInfo(node_coords=node_coords,
                                    face_node_connectivity=face_nodes,
                                    node_start_index=node_start_index)

    # GridInfo: UM data is stored on a regular Grid.
    print("Instantiating GridInfo ...")
    with report_time(times_list):
        grid_info = regrid.GridInfo(lons=longitudes,
                                    lats=latitudes,
                                    lonbounds=lon_bounds,
                                    latbounds=lat_bounds,
                                    circular=True)

    # Regridder: Prepare the regridding.
    # Includes the calculation of the regrid weights.
    print("Instantiating Regridder ...")
    with report_time(times_list):
        rg = regrid.Regridder(src=mesh_info, tgt=grid_info)

    # Now regrid LFRic data from its mesh to UM grid.
    # The regridded_result is a numpy array.
    print("Performing regridding ...")
    with report_time(times_list):
        regridded_result = rg.regrid(src_array=lfric_data)

    time_sum = sum(times_list, timedelta())
    print(f"Sum of times: {time_sum}")


    # PLOT ########################################################################

    print("Plotting results ...")
    plot_info = (node_coords, face_nodes, node_start_index, lfric_data,
                 regridded_result, longitudes, latitudes, lon_bounds, lat_bounds, plotfile_path)
    plot_raw_and_regridded_results(*plot_info)
    print(f'... done :  Plotted results are in the file "{plotfile_path}".')

    print(f"Finished running {__file__}")
