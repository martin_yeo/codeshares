#!/usr/bin/env python
# *****************************COPYRIGHT*******************************
# (C) Crown copyright Met Office. All rights reserved.
# *****************************COPYRIGHT*******************************
"""Generate experimental diagnostics for Fieldcalc by splicing into StaGE
input data.

This script will:

    1. Attempt to extract the umglaa_pa???.gpcs file from MASS.
       If it fails, it will try the next file. The MOOSE_LOC variable controls
       the location on MASS.
    2. Generate a fieldcalc namelist for that forecast time and write to
       disk as (fc_???.nl). The experimental diagnostics will only be output
       three-hourly.
    3. Run fieldcalc using raw UM output kept in INPUT_PATH. This is required
       because the .gpcs files do not contain a field required for one of the
       diagnostics. The Fieldcalc executable is defined by FCALC.
    4. Use Mule to merge the .gpcs file with the fields written by Fieldcalc.
       It will update the times in the header to match those in the .gpcs files
       by using a field in the .gpcs file with the same LBFT lookup header
       value as a template.
    5. The resulting file will be written to disk with a .gpcs.merged
       extension.

To use this script, update the variables mentioned above as required. Then
run the script, which should extract the data from MASS and produce
.gpcs.merged files. You can delete the intermediate files (output_pa??? and
fc_*.nl). Then rename the .gpcs.merged files to .gpcs, overwriting the original
versions.

"""

from __future__ import print_function, division

import mule
import os
import re
import subprocess

# Fieldcalc namelist header
FC_HEADER = """
 !----  Set Header Modification switch  ----
 &PPHdrModNL
            PPHdrMod = .false.
/


"""

# Fieldcalc namelist commands for a single forecast time
FC_LINES = """


 !----  T+00 : 20/073 extended conv rate  ----
 &CommandNL
            Action   = "CLEAR_ERR"
 /

 &CommandNL
            Action   = "CONPPN",
            FCTime   = FCTOKEN,
            Store    = 7,
 /


 &CommandNL
            Action   = "CLEAR_ERR"
 /

 &CommandNL
            Action   = "EXT_CONV",
            FCTime   = FCTOKEN,
            Source   = 7,
            Store    = 20,
 /

 &CommandNL
            Action   = "WRITEFLDS",
            NumFlds  = 1,
            PackType = "NONE",
            PackAcc  =  -99.0,
            Source   = 20,
 /



 &NewPPHdrNL
            NewPPHdr(1:2)%STCode = 20073,
 /

 &CommandNL
            Action   = "BV_FREQ",
            FCTime   = FCTOKEN,
            Store    = 13,14,15,16,17,
 /

 &CommandNL
            Action   = "WSHEAR_SQ",
            FCTime   = FCTOKEN,
            Store    = 8,9,10,11,12,
 /

 &CommandNL
            Action   = "INV_RICH",
            FCTime   = FCTOKEN,
            Source   = 13,14,15,16,17,8,9,10,11,12,
            Store    = 1,2,3,4,5,
 /

 &CommandNL
            Action   = "WRITEFLDS",
            NumFlds  = 5,
            PackType = "NONE",
            PackAcc  =  -99.0,
            Source   = 1,2,3,4,5,
 /


 &NewPPHdrNL
            NewPPHdr(1:5)%STCode = 20076,20076,20076,20076,20076,
 /

 &CommandNL
            Action   = "CONRAIN",
            FCTime   = FCTOKEN,
            Store    = 6,
 /




 &CommandNL
            Action   = "COMBINE",
            FCTime   = FCTOKEN,
            Factor   = 30000.0,1.0,1.0,1.0,1.9,0.2,0.3,0.2,
                       -100.0,100.0,1.0,1.0,0.0,
            Source   = 1,2,3,4,5,6,7,20,
            Store    = 10,11,12,13,14,
 /

 &CommandNL
            Action   = "WRITEFLDS",
            NumFlds  = 5,
            PackType = "NONE",
            PackAcc  =  -99.0,
            Source   = 10,11,12,13,14,
 /

 &NewPPHdrNL
            NewPPHdr(1:5)%STCode = 20077,20077,20077,20077,20077,
 /

"""

# Fieldcalc namelist footer
FC_FOOTER = """

 &CommandNL
            Action   = "END"
 /
"""

# Location of StaGE KGI on Moose
MOOSE_LOC = \
    'moose:/adhoc/projects/hpcdecoupler/decoupler/test_data/glm/PS38_2/T0600Z/'

# Location of head-of-trunk fieldcalc wrapper
FCALC = '/projects/um1/vn10.6/xc40/utilities/um-fieldcalc'

# Type of suite (oper or para)
SUITE_TYPE = 'oper'

# Cycle time
CYCLE = '20161104T0000Z'

# Location of raw UM output (before .gpcs filtering)
INPUT_PATH = '/critical/opfc/suites-{0}/global/share/cycle/{1}/glm_um'.format(
    SUITE_TYPE, CYCLE)


def write_namelist(fname, lines, newline=False):
    '''Write a file given names and contents. The optional newline argument
    adds a newline at the end of each element of the list.'''
    with open(fname, 'w') as fh:
        for line in lines:
            if newline:
                fh.write("{0}\n".format(line))
            else:
                fh.write(line)


def run_command(command):
    '''Given a command as a string, run it and return the exit code, standard
    out and standard error. The optional shell argument allows a shell to
    be spawned to allow multiple commands to be run.'''

    # Turn command into a list
    command_list = command.split()

    # Create the Popen object and connect out and err to pipes
    p = subprocess.Popen(command_list, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    # Do the communicate and wait to get the results of the command
    stdout, stderr = p.communicate()
    rc = p.wait()

    # Reformat stdout
    stdout = ''.join(stdout)
    stdout = stdout.split("\n")

    # Reformat stderr
    stderr = ''.join(stderr)
    stderr = stderr.split("\n")

    return rc, stdout, stderr


def generate_fieldcalc_namelist(fctime):
    """Generate namelist from template for given forecast time."""
    return re.sub(r'FCTOKEN', '{0}'.format(fctime), FC_LINES)


def map_fctime_to_fname(fctime):
    """Map glm forecast time to filename."""
    return (3*((fctime+2) // 3))


def set_time(field, rfield):
    """Reset header times based on reference field."""
    field.lbyr = rfield.lbyr
    field.lbmon = rfield.lbmon
    field.lbdat = rfield.lbdat
    field.lbhr = rfield.lbhr
    field.lbmin = rfield.lbmin
    field.lbsec = rfield.lbsec
    field.lbyrd = rfield.lbyrd
    field.lbmond = rfield.lbmond
    field.lbdatd = rfield.lbdatd
    field.lbhrd = rfield.lbhrd
    field.lbmind = rfield.lbmind
    field.lbsecd = rfield.lbsecd
    return field


def main():
    ftimes = {}
    orog_file = '{0}/umglaa_pa000'.format(INPUT_PATH)

    # Generate namelists
    fctime_to_fname = {}
    for fctime in range(0, 169, 3):
        fctime_to_fname[fctime] = map_fctime_to_fname(fctime)
        if not fctime_to_fname[fctime] in ftimes:
            ftimes[fctime_to_fname[fctime]] = FC_HEADER
        ftimes[fctime_to_fname[fctime]] = ftimes[
            fctime_to_fname[fctime]] + generate_fieldcalc_namelist(
            fctime)

    # Loop over files
    for item in ftimes:
        ftimes[item] = ftimes[item] + FC_FOOTER

        infile1 = '{0}/umglaa_pa{1:03d}'.format(INPUT_PATH, item)
        infile2 = '{0}/umglaa_pb{1:03d}'.format(INPUT_PATH, item)
        out_file = 'output_pa{0:03d}'.format(item)
        gpcs_file = os.path.basename(infile1 + '.gpcs')
        final_file = gpcs_file + '.merged'

        # Retrieve StaGE data from MASS
        moo_cmd = 'moo get {0} {1}'.format(MOOSE_LOC + gpcs_file, gpcs_file)
        if not os.path.isfile(gpcs_file):
            print (moo_cmd)
            rc, stdout, stderr = run_command(moo_cmd)
            if rc:
                print ("Error:", stderr)
                continue

        # Write namelist to disk
        nl_fname = 'fc_{0}.nl'.format(fctime_to_fname[int(item)])
        write_namelist(nl_fname, ftimes[item])

        # Run fieldcalc to generate experimental diagnostics
        fc_cmd = '{5} -n {0} -i {1} -j {2} -l {3} -w -o {4}'.format(
            nl_fname, infile1, infile2, orog_file, out_file, FCALC)
        if os.path.isfile(out_file):
            os.remove(out_file)
        print (fc_cmd)
        rc, stdout, stderr = run_command(fc_cmd)
        if rc:
            print ("Error:", stderr)
            continue

        # Merge StaGE data with experimental diagnostics
        print ("Merging {1} with {0}".format(gpcs_file, out_file))
        ff1 = mule.FieldsFile.from_file(gpcs_file)
        new_ff = ff1.copy()
        ff2 = mule.FieldsFile.from_file(out_file)

        # Modify STASHmaster to be correct and save fctime
        header_save = {}
        meaned_header_save = {}
        for field in ff1.fields:
            if not hasattr(field, 'lbuser4'):
                continue
            if field.lbft not in header_save:
                header_save[field.lbft] = field
            if field.lbuser4 == 20007:
                field.stash.grid = 11
            if field.lbuser4 in [20050, 20051, 20054, 20055, 20052,
                                 20053, 20056, 20057, 20049]:
                field.stash.grid = 1
            if field.lbuser4 == 5206 and field.lbft not in meaned_header_save:
                meaned_header_save[field.lbft] = field
            new_ff.fields.append(field)

        for field in ff2.fields:
            if hasattr(field, 'lbft') and field.lbft in header_save:
                # 20/073 Extended Convective Precip rate is a time-meaned
                # quantity, therefore we need to replace the header with a
                # time-meaned header
                if field.lbuser4 == 20073:
                    field = set_time(field, meaned_header_save[field.lbft])
                else:
                    field = set_time(field, header_save[field.lbft])
                new_ff.fields.append(field)

        # Write merged file
        new_ff.to_file(final_file)


if __name__ == '__main__':
    main()
