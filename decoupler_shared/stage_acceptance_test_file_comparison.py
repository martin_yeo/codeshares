#!/bin/python
'''
----------------------------------------------------------------------

   Simple test of npz data agreement with source fields-files.

----------------------------------------------------------------------
'''

import iris
iris.FUTURE.cell_datetime_objects = True

import numpy as np
from datetime import datetime

from fieldsfile_comparison_functions import *
import re
import glob
import argparse

#  Set iris switch to enable new behaviour concerning reference surfaces and
#  dimensionless vertical coordinates.
iris.FUTURE.netcdf_promote = True

NPZ_STASH_FORM = re.compile('stash_source = [msi"0-9]{12}')
NPZ_TIMES = re.compile('data:\n\n time = [-"0-9T, ]{10,}')
TIMEFORM_HOUR = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}')
TIMEFORM_NOHOUR = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}')
RUN_TIME_FORM = re.compile('/T[0-9]{4}Z')
RUN_ID_FORM = re.compile('/results/[_a-z]*')
CHUNK_NUMBER_FORM = re.compile('_p[bc]{1}(?P<cnum>[0-9]{3})')


def get_decimation_factor(data):
    '''
    Obtain decimation factor of npz data.
    '''
    try:
        return data['decimate']
    except:
        return 1


def get_npz_times_and_stash(cdl_file, npz_files):
    '''
    Extract the stash code, and validity times from a cdl file.
    Return the path to the associated npz file if it exists.
    '''
    npz_file_search = cdl_file.strip('.cdl')
    f_index, = [i for i, f_path in enumerate(
        npz_files) if npz_file_search in f_path]

    with open(cdl_file, 'r') as file_in:
        cdl_data = file_in.read()
        time_information, = re.findall(NPZ_TIMES, cdl_data)
        stash_information = re.search(
            NPZ_STASH_FORM, cdl_data).group(0).split('"')[1]

    timelist = []
    for item in time_information.split('"'):
        if re.match(TIMEFORM_HOUR, item):
            timelist.append(datetime.strptime(item, '%Y-%m-%dT%H'))
        elif re.match(TIMEFORM_NOHOUR, item):
            timelist.append(datetime.strptime(item + 'T00', '%Y-%m-%dT%H'))
    return timelist, stash_information, npz_files[f_index]


def list_fieldsfiles_and_npz(ff_path, npz_path, stash_codes=None):
    '''
    List all the npz, cdl and fields files in the given paths.
    Extract the run time and run id from the paths.
    '''
    fields_files = glob.glob(ff_path + '/*aa_p*')

    if stash_codes:
        cdl_files = []
        npz_files = []
        for stash in stash_codes:
            cdl_path_suffix = '/*_{}*{}_*.cdl'.format(stash[4:6], stash[7:10])
            npz_path_suffix = '/*_{}*{}_*.npz'.format(stash[4:6], stash[7:10])
            cdl_files.extend(glob.glob(npz_path + cdl_path_suffix))
            npz_files.extend(glob.glob(npz_path + npz_path_suffix))
    else:
        cdl_files = glob.glob(npz_path + '/*.cdl')
        npz_files = glob.glob(npz_path + '/*.npz')

    run_time = re.search(RUN_TIME_FORM, ff_path).group(0).split('/')[1]
    run_id = re.search(RUN_ID_FORM, npz_path).group(0).split('/')[2]

    if len(fields_files) == 0:
        print 'Have not found any fields files in ', ff_path
        exit(1)
    else:
        print 'Have found {} fields files.'.format(len(fields_files))

    if len(npz_files) == 0:
        print 'Have not found any npz files in ', npz_path
        exit(1)
    else:
        print 'Have found {} npz files.'.format(len(npz_files))


    return fields_files, cdl_files, npz_files, run_time, run_id


def compare_ff_and_npz(fields_files, cdl_files, npz_files, tolerance,
                       result_file, comparison_type, stash_codes=None):
    '''
    Carry out the data equivalence check between the npz and fields files.
    '''

    #  Counting variable for failures
    fail_count = 0

    ff_stash_constraint = None
    if stash_codes:
        ff_stash_constraint = iris.AttributeConstraint(
            STASH=lambda stash: stash in stash_codes)

    for fields_file in fields_files:
        #  Print current fields file name to output.
        print_current_file(result_file, str(fields_file))

        chunk_number = '{:03d}'.format(int(re.search(
                    CHUNK_NUMBER_FORM,fields_file).group('cnum')))

        #  Catch iris file import warnings to avoid printing unimportant
        #  information to output.
        ff_all = iris.load(fields_file, ff_stash_constraint)

        for cdl_file in cdl_files:
            timelist, npz_stash, npz_file = get_npz_times_and_stash(
                cdl_file, npz_files)
            if not npz_file:
                print 'SKIPPING: No CDL/NPZ pair found for ', npz_stash,
                continue

            npz_data = np.load(npz_file)

            #  Get decimation factor of npz data.
            step = get_decimation_factor(npz_data)

            npz_ap = npz_data['data']

            for ttime, i_npz in zip(timelist, npz_ap):

                constrain_ff = iris.AttributeConstraint(STASH=npz_stash) & \
                    iris.Constraint(time=lambda c: c == ttime)
                try:
                    i_ff = ff_all.extract(constrain_ff)[0]
                except:
                    #  STASH & Time not found in FF, so continue to next one.
                    continue

                #  Decimate data to match output of acceptance tests in npz
                #  files.
                i_ff_deci = decimate(i_ff.data, step).astype(np.float32)

                #  Calculate statistics for matching fields file and NetCDF
                #  cube to compare values.
                comp_max, comp_min, comp_stdev = compare_statistics(
                    i_ff_deci, i_npz, tolerance)

                #  Print ouput depending upon comparison result.
                if comp_max and comp_min and comp_stdev:
                    print_comparison_success(result_file, i_ff.name(), ttime,
                                             npz_stash, npz_file,
                                             comparison_type)
                else:
                    #  If statistics don't match, print details of the cube for
                    #  which the comparison failed.
                    fail_count += 1

                    print_comparison_fail(result_file, i_ff.name(), ttime,
                                          npz_stash,
                                          get_cube_dim_coord_names(i_ff),
                                          get_statistics(i_ff_deci),
                                          get_statistics(i_npz))
                    locate_differences(i_ff_deci, i_npz, 'npz', result_file)

    print_summary(result_file, fail_count)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Compare input fieldsfile and acceptance test output npz.')
    parser.add_argument('ff_path', metavar='ff_path', type=str,
                        help='path to directory containing fields files.')
    parser.add_argument('npz_path', metavar='npz_path', type=str,
                        help='path to directory containing npz files.')
    parser.add_argument('--tolerance', metavar='tolerance', type=float,
                        default=0.0001, help='comparison percentage error '
                        'tolerance e.g. flag differences > 0.001 per cent'
                        ' error.')
    parser.add_argument('--stash', metavar='stash', type=str, nargs='+',
                        help='test only these stash codes, e.g.'
                        'm01s00i409')
    args = parser.parse_args()

    fields_files, cdl_files, npz_files, run_time, run_id = \
        list_fieldsfiles_and_npz(args.ff_path, args.npz_path, args.stash)
    comparison_type = 'npz'
    result_file = 'acceptance_test_results_{}.txt'.format(run_id)
    print '\nResults will be output to : {}.\n'.format(result_file)
    print_header(result_file, comparison_type, run_id,
                 run_time, args.ff_path, args.npz_path)

    compare_ff_and_npz(fields_files, cdl_files, npz_files, args.tolerance,
                       result_file, comparison_type, args.stash)

    print 'Data comparison complete. See output: more {}'.format(result_file)
