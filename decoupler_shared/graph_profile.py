from collections import OrderedDict
from datetime import datetime, timedelta
import re
import argparse
import sys
import os
import glob
import pstats

import numpy as np

import log_parse
from profile_stats import (get_matching_function_data, get_top_level_stats,
                           get_func_stats, get_time_data)
import show_prof

# The pstat.Stats object returned by show_prof.aggregate() is not very well
# doumented, so I will provide some info here.
#
# The object contains information about all the calls made in the program,
# where they happened, the time they took and the number which were made. The
# time taken is aggregated over all calls so it's not possible to determine the
# time taken by a particular method call.
#
# The stats attribute is a dictionary with keys of the form
# (filepath, line, function) and values which are dictionaries. The diction-
# aries have the keys of the same form, and values representing statistics of
# calls to the function defined by the key. These values have the form:
# (primative calls, recursive calls, tottime, cumtime) where tottime and
# cumtime are the same as specified here:
# https://docs.python.org/2.7/library/profile.html#instant-user-s-manual
#
# After a call to calc_callees, the all_callees attribute contains a dictionary
# similar to the above, except that the value dictionary represents all call
# *from* the key function

# The following constants specify the file/function combination
# to use to get the 'saving' and 'loading' statistics output.
# They may need to be changed if the codebase changes, and possibly add a line
# number to distinguish identically named functions in a file.

RE_TOP_LEVEL_FILE = re.compile('.*/bgd/framework.py$')
RE_SAVING_FILE = re.compile('.*/iris/io/__init__.py$')
RE_LOADING_FILE = re.compile('.*/bgd/plugins\.py$')

TOP_LEVEL_FUNC = '_process_single_level'
SAVING_FUNC = 'save'
LOADING_FUNC = '_touch_data'

#other, loading, saving , un-profiled
BAR_COLORS = [ 'yellowgreen', 'firebrick', 'skyblue', 'purple']


def get_elapsed_time(time, individual_diagnostics=None, warn=False):
    if not individual_diagnostics:
        elapsed = sum([t.elapsed for t in time])
    else:
        elapsed = time.elapsed
    return elapsed


def get_time_data_and_elapsed_time(stat, time, individual_diagnostics=None, warn=False):
    elapsed = get_elapsed_time(time, individual_diagnostics)
    saving_time, loading_time, other_time = get_time_data(stat, warn=False)
    return (elapsed, saving_time, loading_time, other_time)


def load_graph_run_data(run_names, log_dirs, grid, profile_dirs=None, profile_files=None,
                        individual_diagnostics=False):
    def profile_files_in(d):
        return sorted(glob.glob(os.path.join(d, '*.pstat')))
    def log_files_in(log_dirs, profile_dirs):
        time_chunk_stash_code = filename_slice(profile_dirs)
        log_files = glob.glob(os.path.join(log_dirs, '*.err'))
        if len(log_files) == 0:
            print '*.err log files not found in {}'.format(log_dirs)
        sorted_log_files = []
        time_chunk_stash_codes = []
        for tcsc in time_chunk_stash_code:
            f = [s for s in log_files if tcsc in s]
            if len(f)>0:
                sorted_log_files.extend(f)
                time_chunk_stash_codes.append(tcsc)
        return sorted_log_files
    def run_names_from_log_files(log_dirs, profile_dirs):
        time_chunk_stash_code = filename_slice(profile_dirs)
        log_files = glob.glob(os.path.join(log_dirs, '*.err'))
        sorted_log_files = []
        time_chunk_stash_codes = []
        for tcsc in time_chunk_stash_code:
            f = [s for s in log_files if tcsc in s]
            if len(f)>0:
                sorted_log_files.extend(f)
                time_chunk_stash_codes.append(tcsc)
        return time_chunk_stash_codes
    def filename_slice(d):
        file_list = sorted(glob.glob(os.path.join(d, '*.pstat')))
        time_chunk_stash_code = []
        for f in file_list:
            f = os.path.basename(f)
            time_chunk_stash_code.append(filename_slice_options(f))
        return time_chunk_stash_code
    def filename_slice_options(f):
        if grid in ["glm_frozen_autosat"]:
            stash_code = "_".join(f.split(".")[0].split("_")[3:])
            tcsc = stash_code
        elif grid in ["glm_frozen_pcs"]:
            time_chunk = f.split("-")[2]
            stash_code = "_".join(f.split(".")[0].split("_")[4:])
            tcsc = "_".join([time_chunk, stash_code])
        else:
            raise IOError("Unrecognised grid specified: {}".format(grid))
        return tcsc


    time_details = [log_parse.load_details(log_files_in(ld, pd))
                    for ld, pd in zip(log_dirs, profile_dirs)]
    
    times = [[time_details[0][t] for t in time_details[0]]]

    if profile_dirs is not None:
        if not individual_diagnostics:
            stats = (show_prof.aggregate(profile_files_in(d)) for d in profile_dirs)
        else:
            stats = [pstats.Stats(f) for d in profile_dirs for f in profile_files_in(d)]
    else:
        stats = (pstats.Stats(f) for f in profile_files)

    if individual_diagnostics:
        run_names = [run_names_from_log_files(ld, pd)
                    for ld, pd in zip(log_dirs, profile_dirs)][0]
        times = times[0]
    result = OrderedDict()

    for run_name, stat, time in zip(run_names, stats, times):
        result[run_name] = get_time_data_and_elapsed_time(
            stat, time, individual_diagnostics=individual_diagnostics)
    result['_part_names'] = \
        ['total_elapsed', 'saving_time', 'loading_time', 'other_time']
    return result, run_names


def slicer(values, start=None, stop=None, step=None):
    one_slice = slice(start, stop, step)
    return values[one_slice]


def timestamps_in_filenames_to_datetimes(profile_dirs):
    def strip_timestamps(profile_dirs):
        def profile_files_in(d):
            return sorted(glob.glob(os.path.join(d, '*')))
        sorted_profile_files = [profile_files_in(d) for d in profile_dirs][0]
        sorted_profile_files = [os.path.basename(f) for f in sorted_profile_files]
        timestamps = [f.split("-")[1] for f in sorted_profile_files]
        return timestamps
    def timestamps_to_datetimes(timestamps):
        datetime_list = []
        for timestamp in timestamps:
            datetime_list.append(datetime.strptime(timestamp, "%Y%m%dT%H%M%SZ%f"))
        return datetime_list
    timestamps = strip_timestamps(args.profile_dirs)
    datetime_list = timestamps_to_datetimes(timestamps)
    return datetime_list


def plot_run_data(run_names, data, versus_timestamps,
                  elapsed_time_only, save, horizontal_bars,
                  datetimes=None, colors=BAR_COLORS):

    # Set matplotlib backend to agg if saving. This can be used on the HPC.
    if save is not None:
        import matplotlib
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates

    part_names = data['_part_names']

    # Make an array of data[run, part].
    data_array = np.array([data[run_name] for run_name in run_names])
    # Reverse and transpose the data,
    # to get data[part, run] with part values in ascending order.
    data_array = data_array[:, ::-1].transpose()
    part_names = part_names[::-1]

    # Work out upper and lower heights of each bar segment:

    # Subtract all other parts from the full "elapsed" time, which includes all
    # of them (whereas they don't include each other)...
    bar_tops = data_array.copy()
    bar_tops[-1] -= np.nansum(data_array[:-1], axis=0)
    # ...and rename 'elapsed' appropriately.
    part_names[-1] = 'un-profiled'

    # Form cumulative sums for the top of each section.
    # NOTE: this makes the *top* of each bar the original 'total elapsed'.
    bar_tops = np.cumsum(bar_tops, axis=0)
    bar_tops[-1][np.isnan(bar_tops[-1])] = data_array[-1][np.isnan(bar_tops[-1])]

    # Make an array of the lower part of each bar
    bar_btms=bar_tops.copy()
    bar_btms[1:] = bar_btms[0:-1]
    bar_btms[0:1] = 0

    if horizontal_bars:
        plt.figure(figsize=(20, 12))
    else:
        plt.figure(figsize=(12, 8))

    # Plot multiple stacked bars, one bar per run with stacked values-per-part:
    # We specify each bar 'part' in turn, as a set of values across all the
    # bars (runs) at once.  This seems frankly counter-intuitive !l
    index_for_removing_nan = ~np.isnan(np.sum(bar_tops - bar_btms, axis=0))
    bar_tops = bar_tops[:, index_for_removing_nan]
    bar_btms = bar_btms[:, index_for_removing_nan]
    datetimes = np.array(datetimes)[index_for_removing_nan]

    if elapsed_time_only:
        bar_tops = np.max(bar_tops, axis=0)
        bar_tops = np.expand_dims(bar_tops, 0)
        bar_btms = np.zeros(bar_tops.shape)
        part_names = ["total_elapsed"]

    n_parts, n_runs = bar_tops.shape
    bars = np.zeros(n_parts, dtype=object)

    width = 0.5
    for i_part in range(0, n_parts):
        if versus_timestamps:
            if horizontal_bars:
                bars[i_part] = plt.plot(
                    bar_tops[i_part] - bar_btms[i_part],
                    datetimes, color=BAR_COLORS[i_part])
            else:
                bars[i_part] = plt.plot(
                    datetimes, bar_tops[i_part] - bar_btms[i_part],
                    color=BAR_COLORS[i_part])
        else:
            if horizontal_bars:
                bars[i_part] = plt.barh(
                    bottom=np.arange(n_runs) + 0.5*width,
                    left=bar_btms[i_part],
                    width=bar_tops[i_part] - bar_btms[i_part],
                    height=width,
                    color=BAR_COLORS[i_part],
                    edgecolor=BAR_COLORS[i_part])
            else:
                bars[i_part] = plt.bar(
                    left=np.arange(n_runs) + 0.5*width,
                    bottom=bar_btms[i_part],
                    height=bar_tops[i_part] - bar_btms[i_part],
                    width=width,
                    color=BAR_COLORS[i_part],
                    edgecolor=BAR_COLORS[i_part])

    if versus_timestamps:
        if horizontal_bars:
            dates_format = mdates.DateFormatter("%H%MZ:%Ss")
            plt.gca().yaxis_date()
            plt.gca().yaxis.set_major_formatter(dates_format)
            plt.yticks()
            plt.grid()
            legend_loc = 'lower right'
            plt.xlabel('Run Time (seconds)')
            plt.ylabel('Timestamp')
        else:
            dates_format = mdates.DateFormatter("%H%MZ:%Ss")
            plt.gca().xaxis_date()
            plt.gca().xaxis.set_major_formatter(dates_format)
            plt.xticks()
            plt.grid()
            legend_loc = 'upper left'
            plt.xlabel('Timestamp')
            plt.ylabel('Run Time (seconds)')
    else:
        if horizontal_bars:
            ticks = slicer(np.arange(n_runs) + 0.5 * width, step=max(int(n_runs/20),1)) + 0.5*width
            plt.yticks(ticks, slicer(run_names, step=max(int(n_runs/20),1)))
            legend_loc = 'lower right'
            plt.ylim([0,n_runs])
            plt.xlabel('Run Time (seconds)')
            plt.ylabel('Diagnostic')
        else:
            ticks = slicer(np.arange(n_runs) + 0.5 * width, step=max(int(n_runs/20),1)) + 0.5*width
            plt.xticks(ticks, slicer(run_names, step=max(int(n_runs/20),1)),
                rotation="vertical")
            legend_loc = 'upper left'
            plt.xlim([0,n_runs])
            plt.xlabel('Diagnostic')
            plt.ylabel('Run Time (seconds)')

    plt.tight_layout()
    plt.legend([bar[0] for bar in bars], part_names, loc=legend_loc)

    if save is not None:
        plt.savefig(save[0])
    else:
        plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Show a graph of profile output.')
    parser.add_argument('--profile_dirs', metavar='dir', type=str,
                        nargs='+',
                        help='A list of directories containing output files '
                             'from the Python profiler.')
    parser.add_argument('--profile_files', metavar='dir', type=str,
                        nargs='+',
                        help='An output file from the Python profiler.')
    parser.add_argument('--log_dirs', metavar='dir', type=str, 
                        nargs='+', required=False,
                        help='A list of directories containing output from '
                             'Unix time and strace commands.')
    parser.add_argument('-i', '--individual_diagnostics', action="store_true",
                        help='Calculate statistics and plot bars for '
                             'the individual diagnostics.')
    parser.add_argument('-v', '--versus_timestamps', action="store_true",
                        help='Enforce that the plot is versus timestamps. '
                             'The default will be against diagnostics ordered '
                             'by time.')
    parser.add_argument('-e', '--elapsed_time_only', action="store_true",
                        help='Plot only the elapsed time, rather than the '
                             'other time constituents. ')
    parser.add_argument('-s', '--save', metavar='filename',
                        type=str, nargs=1,
                        help='Save figure rather than plot to screen. ')
    parser.add_argument('-hb', '--horizontal_bars', action="store_true",
                        help='Plot a horizontal bar chart, in contrast with '
                             'the default vertical bar chart. ')
    parser.add_argument('-g', '--grid', type=str, nargs=1, required=True,
                        help='Plot the specified grid.')
    args = parser.parse_args()

    USE_TEST_DATA = False
    if args.profile_dirs is None and args.profile_files is None:
        # Apply some default directory settings.
        run_names = [
             '0621T00Z_xce_single',
             '0621T00Z_xcf_single',
             '0621T00Z_xce_concurrent',
             '0621T00Z_xcf_concurrent']
        if USE_TEST_DATA:
            data = {
                '0621T00Z_xce_single': (5649.540000000003, 1173.0013339999996, 675.1823090000006, 1.9830579999994598),
                '0621T00Z_xcf_single': (6320.659999999988, 1672.4263550000003, 601.6436480000004, 10.701848999999584),
                '0621T00Z_xce_concurrent': (8099.880000000007, 1105.1395150000003, 718.3504919999989, 34.09028299999932),
                '0621T00Z_xcf_concurrent': (7870.900000000015, 1781.442368000002, 627.783267, 3.9759529999969345),
                '_part_names': ['total_elapsed', 'saving_time', 'loading_time', 'other_time']}
            ADD_EXTRA_RUN = False
            if ADD_EXTRA_RUN:
                run_names += ['test']
                data['test'] = (5000.0, 2000.0, 1500.0, 500.0)
        else:
            base_dir = '/data/users/dkirkham/shared/stage_profiles/'
            dirnames = [os.path.join(base_dir, name) for name in run_names]
            args.profile_dirs = [os.path.join(dirname, 'stats')
                                 for dirname in dirnames]
            args.log_dirs = [os.path.join(dirname, 'times')
                             for dirname in dirnames]
    else:
        if args.profile_dirs is None != args.profile_files is None:
            sys.stderr.write('Must specify either profile_dirs or '
                             'profile_files and not both!\n')
            sys.exit(1)

        # Check that the two lists are the same length
        if args.profile_dirs is not None and \
           len(args.profile_dirs) != len(args.log_dirs):
            sys.stderr.write('Need the same number of profile dirs as log dirs!\n')
            sys.exit(2)

        if args.profile_files is not None and \
           len(args.profile_files) != len(args.log_dirs):
            sys.stderr.write('Need the same number of profile files as log '
                             'dirs!\n')
            sys.exit(3)

        run_names = ['' for _ in args.profile_dirs]
    if not os.path.isdir(args.log_dirs[0]):
        sys.stderr.write("log dir {} is not a directory".format(args.log_dirs[0]))
        sys.exit(2)
    if not os.path.isdir(args.profile_dirs[0]):
        sys.stderr.write("profile dir {} is not a directory".format(args.profile_dirs[0]))
        sys.exit(2)

    datetimes = timestamps_in_filenames_to_datetimes(args.profile_dirs)

    if not USE_TEST_DATA:
        data, run_names = load_graph_run_data(
            run_names, args.log_dirs,
            args.grid[0],
            profile_dirs=args.profile_dirs,
            profile_files=args.profile_files,
            individual_diagnostics=args.individual_diagnostics)

    print 'Results {}'.format(data['_part_names'])
    if len(run_names) == 0:
        sys.stderr.write("Error:  Can not plot run data, list of run_names is empty")
        sys.exit(2)
    for run_name in run_names:
        np.set_printoptions(precision=4)
        print '  {!s} : {}'.format(run_name.rjust(10),
                                   np.array(data[run_name]))
    np.set_printoptions()

    # Override the versus_timestamps variable, as this variable
    # is only valid for individual_diagnostics.
    versus_timestamps = args.versus_timestamps
    individual_diagnostics = args.individual_diagnostics
    if not individual_diagnostics:
        versus_timestamps = None

    plot_run_data(
        run_names, data, versus_timestamps,
        args.elapsed_time_only, args.save, args.horizontal_bars,
        datetimes=datetimes)
