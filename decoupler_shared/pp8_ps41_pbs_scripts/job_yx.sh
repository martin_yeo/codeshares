#!/bin/sh
#PBS -N yx 
#PBS -j oe
#PBS -l walltime=02:00:00
#PBS -q shared
#PBS -l select=1:ncpus=1 

PYTHON=/home/d03/itwl/miniconda/envs/bgd/bin/python
BOOST=/home/d03/itwl/projects/git/decoupler/ff_data_hires.py
IPATH=/data/d03/itwl/decoupler/compression/20150929T0000Z
IFILE=${IPATH}/umgl.pp8
STASH="m01s00i023:m01s00i024:m01s00i031:m01s00i033:m01s00i409:m01s03i209:m01s03i210:m01s16i222"
OFILE=${WORKING}/decoupler/yx.out
DFILE=${OFILE}.done

${PYTHON} ${BOOST} ${IFILE} ${STASH} 2560 1920 ${OFILE}

if [ ${?} -eq 0 ]
then
    mv ${OFILE} ${DFILE}
fi
