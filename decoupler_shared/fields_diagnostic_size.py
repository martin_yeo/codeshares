"""
Calculate the FF payload associated with one or more stash-code + lbproc items.

Does not include the footprint of any FF specific headers (for now).

"""

import os

import iris.experimental.um as um
import iris.fileformats.pp as pp


__all__ = ['ff_diagnostic_size']


def _ff_scan(fname, codes, records=None, periods=None):
    class _StashRecord(object):
        def __init__(self, lbuser7, lbuser4, lbproc):
            self.lbuser7 = lbuser7
            self.lbuser4 = lbuser4
            self.lbproc = lbproc
            # Number of fields
            self.fcount = 0
            # Size of a single field header
            self.hsize = 0
            # Cumulative uncompressed field data size
            self.dsize = 0
            # Cumulative extra field data size
            self.esize = 0
            # Number of fields with extra data
            self.ecount = 0

        def _header_size(self, field):
            i = field.int_headers
            r = field.real_headers
            result = (i.dtype.itemsize * i.size) + (r.dtype.itemsize * r.size)
            return result

        def _data_size(self, field):
            # Instead, could use the following, but it reads the actual data
            # which is a performance hit ...
            # result = len(field._get_raw_payload_bytes())
            result = ((field.lbnrec * 2) - 1) * um._WGDOS_SIZE
            return result

        def _extra_size(self, field):
            result = field.lbext * pp.PP_WORD_DEPTH
            return result

        def add(self, field):
            self.fcount += 1
            if self.hsize == 0:
                self.hsize = self._header_size(field)
            self.dsize += self._data_size(field)
            esize = self._extra_size(field)
            if esize != 0:
                self.esize += esize
                self.ecount += 1

        @property
        def total(self):
            return (self.fcount * self.hsize) + self.dsize + self.esize

    if records is None:
        records = {}

    ffv = um.FieldsFileVariant(fname)

    s = set()

    for field in ffv.fields:
        if hasattr(field, 'lbuser7'):
            code = (field.lbuser7, field.lbuser4, field.lbproc)
            if code in codes and (periods is None or
                                  (field.lbmin == 0 and field.lbmind == 0 and (field.lbhr - field.lbhrd) in periods)):
                s.add(field.lbtim)
                r = records.setdefault(code, _StashRecord(*code))
                r.add(field)
    if s:
        print sorted(s)
    return records


def ff_diagnostic_size(filepaths, fieldcodes, periods=None):
    records = {}
    grand = 0
    for fname in filepaths:
        print fname
        _ff_scan(fname, fieldcodes, records=records, periods=periods)

    for k in sorted(records):
        record = records[k]
        msg = 'code = {} count = {} size = {} bytes'
        print msg.format(k, record.fcount, record.total)
        grand += record.total

    print '\ntotal = {} bytes'.format(grand)


def autosat_frozen_glm():
    paths = ['/critical/fpos2/cylc-run/mi-af228/share/data/20151109T0600Z/glm_um/umgl.pp8']
    codes = [(1, 10, 0),
             (1, 12, 0),
             (1, 23, 0),
             (1, 24, 0),
 #            (1, 31, 0),
 #            (1, 33, 0),
             (1, 254, 0),
             (1, 266, 0),
             (1, 267, 0),
             (1, 268, 0),
             (1, 408, 0),
             (1, 409, 0),
             (1, 3209, 0),
             (1, 3210, 0),
             (1, 5212, 0),
             (1, 5213, 0),
             (1, 16004, 0),
#             (1, 16201, 0),
             (1, 16222, 0),
             ]
    periods = range(4, 14)
    ff_diagnostic_size(paths, codes, periods)

    codes = [(1, 31, 0),
             (1, 33, 0),
             (1, 16201, 0),
             ]
    periods = range(0, 4)
    ff_diagnostic_size(paths, codes, periods)


def autosat_frozen_ukv():
    ukv_um = '/critical/fpos2/cylc-run/mi-af251/share/data/20151106T0900Z/ukv_um'
    paths = []
    for stream in ['pb', 'pc']:
        for chunk in range(4, 14, 2):
            paths.append(os.path.join(ukv_um, 'umqvaa_{}{:03d}'.format(stream, chunk)))
    codes = [(1, 23, 0),
             (1, 24, 0),
             (1, 31, 0),
             (1, 33, 0),
             (1, 3209, 0), 
             (1, 3210, 0),
             (1, 16222, 0),
             (1, 10, 0),
             (1, 12, 0),
             (1, 254, 0),
             (1, 266, 0),
             (1, 267, 0),
             (1, 268, 0),
             (1, 408, 0),
             (1, 409, 0),
             (1, 9229, 0),
             (1, 16004, 0),
             (1, 16201, 0),
             ]
    periods = range(3, 13)
    ff_diagnostic_size(paths, codes, periods)
