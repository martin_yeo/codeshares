"""
Pass through data feed for GLOBAL model.

"""
import os

from bgd.plugins import *


TARGET_PATH = os.environ.get('GLM_STAGE_INPUT_DIR', '/data/d04/ithr/decoupler/global/2015051300')

TIME_CHUNK = int(os.environ.get('STAGE_TIME_CHUNK', '6'))

INPUT_PATHS = {
    'umglaa_pa': os.path.join(TARGET_PATH, 'umglaa_pa{:03d}.calc'.format(TIME_CHUNK)),
    'umglaa_pb': os.path.join(TARGET_PATH, 'umglaa_pb{:03d}'.format(TIME_CHUNK)),
}

OUTPUT_PATH = os.path.join(os.environ['DATADIR'], 'decoupler/global_frozen/')

ATTRIBUTES = {'grid_id': 'glm_frozen'}

LEVEL1 = {
    '01_00012_0': StructuredFFLoad('umglaa_pb', 'm01s00i012'),
    '01_16004_0': StructuredFFLoad('umglaa_pb', 'm01s16i004'),
    '01_16222_0': StructuredFFLoad('umglaa_pa', 'm01s16i222'),
    '01_00254_0': StructuredFFLoad('umglaa_pb', 'm01s00i254'),
    '01_00266_0': StructuredFFLoad('umglaa_pb', 'm01s00i266'),
    '01_00002_0': StructuredFFLoad('umglaa_pb', 'm01s00i002'),
    '01_03236_0': StructuredFFLoad('umglaa_pa', 'm01s03i236'),
    '01_03281_0': StructuredFFLoad('umglaa_pa', 'm01s03i281'),
    '01_00408_0': StructuredFFLoad('umglaa_pb', 'm01s00i408'),
    '01_04203_0': StructuredFFLoad('umglaa_pa', 'm01s04i203'),
}

# CDI has very specific requirements. We attach those to the processsed data.
LEVEL1 = {name: EnsureDimension(
                    SetAttributes(
                        ConvertTimeToSeconds(
                            ConvertModelLevelNumber(config)
                        ),
                        ATTRIBUTES
                    ),
                    'time'
                )
          for name, config in LEVEL1.items()}

LEVEL2 = {}

LEVEL3 = {}

