from __future__ import print_function
import pstats
import sys
import os
import subprocess
import tempfile
import glob

CHUNK_SIZE=200

def aggregate(filenames):
    # The Stats class can be initialised with a list of filenames. However,
    # this causes a stack overflow if there are too many
    stats = pstats.Stats(filenames[0])
    i = 1
    n = len(filenames)
    for j in xrange(CHUNK_SIZE, n, CHUNK_SIZE):
        stats.add(*filenames[i:j])
        print('\r{}%'.format(i * 100 / n), end="")
        sys.stdout.flush()
        i = j
    stats.add(*filenames[i:])
    print('\r100%')
    return stats

if __name__ == '__main__':
    keep_file = False
    if len(sys.argv) > 2 and sys.argv[1] == '-s':
        save_loc = sys.argv[2]
        keep_file = True
        filenames = sys.argv[3:]
    else:
        filenames = sys.argv[1:]
    if len(filenames) > 0:
        if not keep_file:
            _, save_loc = tempfile.mkstemp()
        stats = aggregate(filenames)
        stats.dump_stats(save_loc)
        try:
            subprocess.call(['snakeviz', '-b', 'firefox', save_loc])
        except KeyboardInterrupt:
            pass
        finally:
            if not keep_file:
                os.remove(save_loc)
