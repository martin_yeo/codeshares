import sys
import re
import glob
from collections import namedtuple, OrderedDict
import warnings

import numpy as np

# Script to parse the output of profiling decoupler, where the call to the
# framework is prepended with "time strace -c "
# Thus, the script expects to find the strace output then the time output

PRE = 0
STRACE = 1
TIME = 2
DONE = 3

# strace output format:
# % time     seconds  usecs/call     calls    errors syscall
# ------ ----------- ----------- --------- --------- ----------------
#  84.31    0.036030          20      1775      1487 stat

STRACE_LIM = 99.0
RE_STRACE_DIV = re.compile(r'^------\.*')
RE_STRACE_LINE = re.compile(r'^\s*(?P<time>\d+\.\d{2})'
                            r'\s+(?P<seconds>\d+\.\d{6})'
                            r'\s+(?P<usecs_call>\d+)\s+(?P<calls>\d+)'
                            r'\s+(?P<errors>\d+)\s+(?P<syscall>\w+)\s*$')

# time output format:
# 0.62user 0.48system 0:02.93elapsed 37%CPU (0avgtext+0avgdata 59548maxresident)k
# 0inputs+0outputs (0major+46983minor)pagefaults 0swaps

RE_TIME = re.compile(r'^(?P<user>\d+\.\d{2})user (?P<system>\d+\.\d{2})system '
                     r'(?P<elapsed_m>\d+):(?P<elapsed_s>\d{2}\.\d{2})elapsed.*'
                     )

StraceDetails = namedtuple('StraceDetails', ['syscall', 'time', 'seconds',
                                             'usecs_call', 'calls', 'errors'])
TimeDetails = namedtuple('TimeDetails', ['user', 'system', 'elapsed'])


def try_parse_strace_line(line):
    m = RE_STRACE_LINE.match(line)
    if m:
        syscall = m.group('syscall')
        time = float(m.group('time'))
        seconds = float(m.group('seconds'))
        usecs_call = int(m.group('usecs_call'))
        calls = int(m.group('calls'))
        errors = int(m.group('errors'))
        return StraceDetails(
            syscall, time, seconds, usecs_call, calls, errors)


def try_parse_time_line(line):
    m = RE_TIME.match(line)
    if m:
        user = float(m.group('user'))
        system = float(m.group('system'))
        elapsed_m = int(m.group('elapsed_m'))
        elapsed_s = float(m.group('elapsed_s'))
        elapsed = 60 * elapsed_m + elapsed_s
        return TimeDetails(user, system, elapsed)


def load_details(filenames, strace=False, last_two_lines=False):
    """
    Parameters:
    strace
        Produce strace details
    last_two_lines
        Interpret the last two lines of the file as time output

    """
    details = OrderedDict()

    for fname in filenames:
        f = open(fname)
        if strace:
            stat = PRE
        else:
            stat = TIME
        strace_sum = 0.0
        strace_details = {}
        time_details = None

        lines = f.readlines()
        if last_two_lines:
            lines = lines[-2:]

        for line in lines:
            if stat == PRE:
                if RE_STRACE_DIV.match(line):
                    stat = STRACE
            elif stat == STRACE:
                if RE_STRACE_DIV.match(line):
                    stat = TIME
                elif strace_sum < STRACE_LIM:
                    strace_det = try_parse_strace_line(line)
                    if strace_det is not None:
                        strace_details[strace_det.syscall] = strace_det
                        strace_sum += strace_det.time
            elif stat == TIME:
                time_details = try_parse_time_line(line)
                if time_details is not None:
                    stat = DONE

        if stat != DONE:
            msg = '{}: Something went wrong: {{}}'.format(fname)
            if stat == PRE:
                msg = msg.format('Could not find strace output\n')
            elif stat == STRACE:
                msg = msg.format('Could not find end of strace output\n')
            elif stat == TIME:
                msg = msg.format('Could not find time output\n')
            warnings.warn(msg)
        else:
            if strace:
                details[fname] = (strace_details, time_details)
            else:
                details[fname] = time_details
    return details


if __name__ == '__main__':
    details = load_details(sys.argv[1:])

    u_times = (t.user for _, t in details.itervalues())
    s_times = (t.system for _, t in details.itervalues())
    e_times = (t.elapsed for _, t in details.itervalues())
    sum_syscalls = (sum(s.seconds for s in ss.itervalues())
                    for ss, _ in details.itervalues())

    tot_u_time = sum(u_times)
    tot_s_time = sum(s_times)
    tot_e_time = sum(e_times)
    tot_syscall_time = sum(sum_syscalls)

    print "User: {}, System: {}, Elapsed: {}, Sys Calls: {}".format(tot_u_time, tot_s_time, tot_e_time, tot_syscall_time)
