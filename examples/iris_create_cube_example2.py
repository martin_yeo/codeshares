"""
Creating cubes example 2
------------------------

In this example, dummy data is setup then added to an Iris Cube.
Cubes are created for data from differing times and ensemble realizations.  
A single dataset is created from the cubes.

The example includes:
 * creating a cube
 * adding bounds to a coordinate
 * a coordinate reference system with lat/lons 
 * creating/adding dimension coordiantes
 * creating/adding auxiliary coordinates associated with a data axis
 * creating/adding auxiliary coordinates not with a data axis (aka scalar coordinates)
 * promoting an auxilary coordinate to a dimension coordinate
 * units and calendars with time information
 * copying coordinates

 * merging and concatenating of single time Cubes into time series data

  See operational StaGE example for more examples of creating cubes with different
  coordinate reference systems:
  https://github.com/MetOffice/stage/blob/master/lib/config/mo_engl_standard_v1.py
  https://github.com/MetOffice/stage/blob/master/lib/config/mo_ukvx_standard_v1.py


"""


import numpy as np
import iris
from iris.coords import DimCoord, AuxCoord
from iris.coord_systems import GeogCS
from iris.fileformats.pp import EARTH_RADIUS
from cf_units import Unit
import datetime

import matplotlib.pyplot as plt


def _make_cube(data, lons, lats, cs, model_levels, realization, time, frt, time_unit):
    """
    Create a cube from (longitude, latitude, model_level) data.
    Add realization dimension, time and forecast reference information, calculate forecast_period.

    """
    cube = iris.cube.Cube(data, long_name="test data")
    # Add lons, lats, model levels - as dimension coordinates (in line with the shape of the data)
    lon_coord = DimCoord(lons, "longitude", units="degrees", coord_system=cs)
    lat_coord = DimCoord(lats, "latitude", units="degrees", coord_system=cs)
    lon_coord.guess_bounds()
    lat_coord.guess_bounds()
    levels_coord = DimCoord(model_levels, long_name="model_levels")
    cube.add_dim_coord(levels_coord, 0)
    cube.add_dim_coord(lat_coord, 1)
    cube.add_dim_coord(lon_coord, 2)

    # Add pressure - as auxiliary coordinate associated with model_levels (on axis 0)
    pressure_coord = AuxCoord([30, 20, 10], long_name="pressure", units="hPa")
    cube.add_aux_coord(pressure_coord, 0)

    # Add realization
    realization_coord = AuxCoord(realization, standard_name="realization", units="1")
    cube.add_aux_coord(realization_coord)
    # promote realization to be axis 0
    # (This is useful as it prevents realization becoming "muddled up" with other scalar information when merging cubes).
    cube = iris.util.new_axis(cube, realization_coord)

    # Add time - as a scalar coordinate because the data does not have a time axis
    # Note: Scalar coordinates are added as AuxCoords but are not associated with an axis
    # Note: time is type float64
    time_coord = AuxCoord(np.array([time], dtype=np.float64), "time", units=time_unit)
    cube.add_aux_coord(time_coord)
    # Add forecast reference time - as a scalar coordinate
    frt_coord = AuxCoord(
        np.array([frt], dtype=np.float64), standard_name="forecast_reference_time", units=time_unit
    )
    cube.add_aux_coord(frt_coord)

    # Add forecast period
    # Note: Example of copying a coordinate
    if time_coord.units == frt_coord.units:
        fp_coord = time_coord.copy()
        fp_coord.standard_name = "forecast_period"
        fp_coord.points = time - frt
        fp_coord.units = "hours"
        cube.add_aux_coord(fp_coord)

    return cube


if __name__ == "__main__":

    # Create dummy data
    # longitude and latitude values and the associated coordinate system
    # longitudes
    # in this example the lons values are the centres of the grid cells
    lowerbnd, upperbnd, npoints = -180, +180, 4
    gridstep = (upperbnd - lowerbnd) / (npoints)
    lons = np.linspace(lowerbnd + 0.5 * gridstep, upperbnd - 0.5 * gridstep, npoints)
    # latitudes
    # in this example the lats values are the centres of the grid cells
    lowerbnd, upperbnd, npoints = -90, +90, 6
    gridstep = (upperbnd - lowerbnd) / (npoints)
    lats = np.linspace(lowerbnd + 0.5 * gridstep, upperbnd - 0.5 * gridstep, npoints)
    cs = GeogCS(EARTH_RADIUS)
    # vertical levels
    model_levels = np.array([0, 1, 2])

    # Data: 3D data (model_levels x nlats x nlons)
    data = np.arange(model_levels.size * lats.size * lons.size).reshape(model_levels.size, lats.size, lons.size)

    # Other dummy data information
    # time and forecast reference time information
    time_unit = Unit("hours since 1970-01-01 00:00:00", calendar="gregorian")
    time = time_unit.date2num(datetime.datetime(2015, 6, 2, 0, 0))  # time = 398112.0
    frt = time
    # Realization
    r = 0

    # Create Iris cubes from different data
    # Realization 0
    cube1 = _make_cube(data, lons, lats, cs, model_levels, r, time, frt, time_unit)      # T+0
    cube2 = _make_cube(data, lons, lats, cs, model_levels, r, time + 6, frt, time_unit)  # T+6
    cube3 = _make_cube(data, lons, lats, cs, model_levels, r, time + 12, frt, time_unit)  # T+12
    # Realization 1
    cube4 = _make_cube(data, lons, lats, cs, model_levels, r + 1, time, frt, time_unit)      # T+0
    cube5 = _make_cube(data, lons, lats, cs, model_levels, r + 1, time + 6, frt, time_unit)  # T+6
    cube6 = _make_cube(data, lons, lats, cs, model_levels, r + 1, time + 12, frt, time_unit)  # T+12

    # Examples of merging different cubes together
    print("Example data from realization 0:  merge the times together")
    cubes = iris.cube.CubeList([cube1, cube2, cube3]).merge()
    print(cubes)

    print("Example merging cubes from: realization 0 T+0 and T+6, and realization 1, T+6 and T+12")
    cubes = iris.cube.CubeList([cube1, cube2, cube5, cube6]).merge()
    print(cubes)

    print("Example merging all the realizations from all the times:  merge the times together and concenate the realizations")
    cubes = iris.cube.CubeList([cube1, cube2, cube3, cube4, cube5, cube6]).merge().concatenate()
    print(cubes)
