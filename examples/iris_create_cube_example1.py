"""
Create dummy data, add it to an iris cube and tweak the forecast reference time information.

# See operational StaGE example for more examples of adding time coordinates to cubes.
# https://github.com/MetOffice/stage/blob/master/lib/stage/tests/unit_tests/plugins/core/test_ConvertTimeToSeconds.py

"""


import numpy as np
import iris
from cf_units import Unit
import datetime


def _make_cube(data, lons, lats):
    """
    Create a 2D cube of longitude, latitude data.
    """
    cube = iris.cube.Cube(data, long_name="test data")
    # Add lons and lats
    lon_coord = iris.coords.DimCoord(lons, "longitude", units="degrees")
    lat_coord = iris.coords.DimCoord(lats, "latitude", units="degrees")
    lon_coord.guess_bounds()
    lat_coord.guess_bounds()
    cube.add_dim_coord(lon_coord, 1)
    cube.add_dim_coord(lat_coord, 0)
    return cube


if __name__ == "__main__":

    # Create dummy data
    # longitudes
    # in this example the lons values are the centres of the grid cells
    lowerbnd, upperbnd, npoints = -180, +180, 4
    lons = np.linspace(lowerbnd, upperbnd, npoints + 1, endpoint=True)[:-1] + (
        (upperbnd - lowerbnd) / (2.0 * npoints)
    )
    # latitudes
    # in this example the lats values are the centres of the grid cells
    lowerbnd, upperbnd, npoints = -90, +90, 6
    lats = np.linspace(lowerbnd, upperbnd, npoints + 1, endpoint=True)[:-1] + (
        (upperbnd - lowerbnd) / (2.0 * npoints)
    )
    # Data: 2D data (nlats x nlons)
    data = np.arange(lats.size * lons.size).reshape(lats.size, lons.size)

    # Create a cube with longitude, latitude
    cube = _make_cube(data, lons, lats)

    # Add forecast reference time
    unit = Unit("hours since 1970-01-01 00:00:00", calendar="gregorian")
    frt = unit.date2num(datetime.datetime(2015, 6, 2, 0, 0))  # frt = 398112.0
    forecast_ref_time_coord = iris.coords.AuxCoord(
        frt, units=unit, standard_name="forecast_reference_time"
    )
    cube.add_aux_coord(forecast_ref_time_coord)

    # Alter the forecast reference time to a different date
    frt = frt + 6  # 2015-06-18 00:00:00
    forecast_ref_time_coord = iris.coords.AuxCoord(
        frt,
        units=Unit("hours since 1970-01-01 00:00:00", calendar="gregorian"),
        standard_name="forecast_reference_time",
    )
    cube.remove_coord("forecast_reference_time")
    cube.add_aux_coord(forecast_ref_time_coord)

    # Or tweak the value of the frt
    cube.coords("forecast_reference_time")[0].points += 6

    print("The cube...")
    print(cube)
